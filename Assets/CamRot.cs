﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamRot : MonoBehaviour
{
    Vector2 angle;
    public float rotSpeed = 200;
    // Start is called before the first frame update
    void Start()
    {
        angle.x = transform.eulerAngles.x;
        angle.y = transform.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {
        float x = -Input.GetAxis("Mouse Y");
        float y = Input.GetAxis("Mouse X");

        angle.x += x * rotSpeed * Time.deltaTime;
        angle.y += y * rotSpeed * Time.deltaTime;
        transform.eulerAngles = angle;
    }
}
