﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

// EventSystem 객체에 붙일 컴포넌트
public class VRInput : BaseInput
{
    public Camera eventCamera = null;

    protected override void Awake()
    {
        GetComponent<BaseInputModule>().inputOverride = this;
    }

    public override bool GetMouseButton(int button)
    {
        return Input.GetButton("Fire1");
    }

    public override bool GetMouseButtonDown(int button)
    {
        return Input.GetButtonDown("Fire1");
    }

    public override bool GetMouseButtonUp(int button)
    {
        return Input.GetButtonUp("Fire1");
    }

    public override Vector2 mousePosition
    {
        get
        {
            return new Vector2(eventCamera.pixelWidth / 2, eventCamera.pixelHeight / 2);
        }
    }
}
