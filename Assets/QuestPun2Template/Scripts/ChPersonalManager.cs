﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using RootMotion.FinalIK;
using BNG;
//
//For handling local objects and sending data over the network
//
namespace Networking.Pun2
{
    public class ChPersonalManager : MonoBehaviourPunCallbacks
    {
        [SerializeField] GameObject CharacterPrefab;
        [SerializeField] GameObject CharacterPrefab1;
        [SerializeField] GameObject CharacterPrefab2;
        [SerializeField] GameObject Player;
        [SerializeField] Transform[] ikHeadTarget;
        [SerializeField] Transform[] ikRightHandTarget;
        [SerializeField] Transform[] ikLeftHandTarget;
        [SerializeField] Transform[] spawnPoints;
        [SerializeField] GameObject inventory;
        [SerializeField] JY_Controller_0123 jyCtrlL;
        [SerializeField] JY_Controller_0123 jyCtrlR;
        //Tools
        List<GameObject> Players;
        int currentToolR;
        int currentToolL;

        private void Awake()
        {
            /// If the game starts in Room scene, and is not connected, sends the player back to Lobby scene to connect first.
            if (!PhotonNetwork.NetworkingClient.IsConnected)
            {
                SceneManager.LoadScene("CHtestPhoton2Room");
                return;
            }
            /////////////////////////////////

            Players = new List<GameObject>();

            if (PhotonNetwork.LocalPlayer.ActorNumber <= spawnPoints.Length)
            {
                //Player = Instantiate();
                Player.transform.position = spawnPoints[PhotonNetwork.LocalPlayer.ActorNumber - 1].transform.position;
                Player.transform.rotation = spawnPoints[PhotonNetwork.LocalPlayer.ActorNumber - 1].transform.rotation;
            }
            
        }

        private void Start()
        {

            if (PhotonNetwork.LocalPlayer.ActorNumber == 1)
            {
                //0208JY추가
                Debug.Log("ActorNumber1 생성");
                PlayerChracter(CharacterPrefab, 2);
                GameObject obj = (PhotonNetwork.Instantiate(inventory.name, Vector3.zero, Quaternion.identity));
                //Player.transform.Find("InvOnOff").GetComponent<InvBtn>().inventory = obj;
                jyCtrlL.ownInv = obj;
                jyCtrlR.ownInv = obj;
                Debug.Log("왼손과 오른손에 인벤토리 생성해 obj로 넣어줌");
                obj.transform.GetChild(2).GetComponent<Canvas>().worldCamera = GameObject.Find("InputModule").GetComponent<Camera>();

            }
            else if (PhotonNetwork.LocalPlayer.ActorNumber == 2)
            {
                PlayerChracter(CharacterPrefab1, 1);
                GameObject obj = (PhotonNetwork.Instantiate(inventory.name, Vector3.zero, Quaternion.identity));

                //JY추가
                jyCtrlL.ownInv = obj;
                jyCtrlR.ownInv = obj;
                obj.transform.GetChild(2).GetComponent<Canvas>().worldCamera = GameObject.Find("InputModule").GetComponent<Camera>();

            }
            else if (PhotonNetwork.LocalPlayer.ActorNumber == 3)
            {
                PlayerChracter(CharacterPrefab2, 2);
                GameObject obj = (PhotonNetwork.Instantiate(inventory.name, Vector3.zero, Quaternion.identity));

                //JY추가
                jyCtrlL.ownInv = obj;
                jyCtrlR.ownInv = obj;
                obj.transform.GetChild(2).GetComponent<Canvas>().worldCamera = GameObject.Find("InputModule").GetComponent<Camera>();

            }


            #region Temporary
            //GameObject obj = (PhotonNetwork.Instantiate(CharacterPrefab.name, Player.transform.position - Vector3.up
            //    , Player.transform.rotation, 0));
            //obj.transform.parent = Player.transform.GetChild(0).transform;
            //GameObject ikET = obj.transform.Find("IKEyeTarget (1)").transform.gameObject;
            //GameObject ikLT = obj.transform.Find("IKLeftTarget (1)").transform.gameObject;
            //GameObject ikRT = obj.transform.Find("IKRightTarget (1)").transform.gameObject;

            //ikET.transform.parent = ikHeadTarget.transform.parent;
            //ikLT.transform.parent = ikLeftHandTarget.transform.parent;
            //ikRT.transform.parent = ikRightHandTarget.transform.parent;

            //ikET.transform.position = ikHeadTarget.position;
            //ikLT.transform.position = ikLeftHandTarget.position;
            //ikRT.transform.position = ikRightHandTarget.position;
            #endregion
        }

        private void PlayerChracter(GameObject characterPrefab, int num)
        {
            GameObject obj = (PhotonNetwork.Instantiate(characterPrefab.name, Player.transform.position - Vector3.up
                , Player.transform.rotation, 0));
            obj.transform.parent = Player.transform.GetChild(0).transform;

            GameObject ikET = obj.transform.Find("IKEyeTarget").transform.gameObject;
            GameObject ikLT = obj.transform.Find("IKLeftTarget").transform.gameObject;
            GameObject ikRT = obj.transform.Find("IKRightTarget").transform.gameObject;

            ikET.transform.parent = ikHeadTarget[num].transform.parent;
            ikLT.transform.parent = ikLeftHandTarget[num].transform.parent;
            ikRT.transform.parent = ikRightHandTarget[num].transform.parent;

            ikET.transform.position = ikHeadTarget[num].position;
            ikLT.transform.position = ikLeftHandTarget[num].position;
            ikRT.transform.position = ikRightHandTarget[num].position;

        }


        //Detects input from Thumbstick to switch "hand tools"
        private void Update()
        {
            //if (OVRInput.GetUp(OVRInput.Button.PrimaryThumbstick))
            //if (OVRInput.GetUp(OVRInput.Button.SecondaryThumbstick))
        }

        //If disconnected from server, returns to Lobby to reconnect
        public override void OnDisconnected(DisconnectCause cause)
        {
            base.OnDisconnected(cause);
            SceneManager.LoadScene(0);
        }
    }
}
