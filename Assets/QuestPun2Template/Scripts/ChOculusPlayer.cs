﻿using UnityEngine;
//
//For easily accesing local head and hand anchors
//
namespace Networking.Pun2
{
    public class ChOculusPlayer : MonoBehaviour
    {
        public GameObject head;
        public GameObject rightHand;
        public GameObject leftHand;

        public static ChOculusPlayer instance;

        private void Awake()
        {
            if (instance == null)
                instance = this;
        }

        private void OnDestroy()
        {
            if (instance == this)
                instance = null;
        }
    }
}
