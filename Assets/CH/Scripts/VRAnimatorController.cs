﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;
using BNG;
using Photon.Pun;

public class VRAnimatorController : MonoBehaviour
{
    public float speedTreshold = 0.1f;
    [Range(0, 1)]
    public float smoothing = 1;
    private Animator animator;
    private Vector3 previousPos;
    private VRIK vrIK;


    OVRPlayerController playerController;
    public float walkSpeed = 0.18f;
    public float runSpeed = 0.4f;
    PhotonView pv;
    InputBridge input;

    void Start()
    {
        animator = GetComponent<Animator>();
        vrIK = GetComponent<VRIK>();
        previousPos = vrIK.solver.spine.headTarget.position;
        pv = GetComponent<PhotonView>();

        if (pv.IsMine)
        {
            playerController = GetComponentInParent<OVRPlayerController>();
            input = GetComponentInParent<InputBridge>();
        }



    }

    void Update()
    {
        Vector3 headsetSpeed = (vrIK.solver.spine.headTarget.position - previousPos) / Time.deltaTime;
        headsetSpeed.y = 0;
        Vector3 headsetLocalSpeed = transform.InverseTransformDirection(headsetSpeed);
        previousPos = vrIK.solver.spine.headTarget.position;

        float previousDirectionX = animator.GetFloat("DirectionX");
        float previousDirectionY = animator.GetFloat("DirectionY");



        //print(previousDirectionY);
        if (input)
        {
            float directionX = input.LeftThumbstickAxis.x;
            float directionY = input.LeftThumbstickAxis.y;
            // input을 사용한 애니메이션 제어
            if (input.LeftThumbstick)
            {
                animator.SetBool("isRun", true);
                playerController.Acceleration = runSpeed;
            }
            else
            {
                animator.SetBool("isRun", false);
                playerController.Acceleration = walkSpeed;
                playerController.BackAndSideDampen = 0.91f;
            }

            animator.SetBool("isMoving",Mathf.Abs(directionX) > speedTreshold || Mathf.Abs(directionY) > speedTreshold);
            animator.SetFloat("DirectionX", Mathf.Lerp(previousDirectionX, directionX, smoothing));
            animator.SetFloat("DirectionY", Mathf.Lerp(previousDirectionY, directionY, smoothing));
        }

        //animator.SetBool("isMoving", headsetLocalSpeed.magnitude > speedTreshold);
        //animator.SetFloat("DirectionX", Mathf.Lerp(previousDirectionX, Mathf.Clamp(headsetLocalSpeed.x, -1, 1), smoothing));
        //animator.SetFloat("DirectionY", Mathf.Lerp(previousDirectionY, Mathf.Clamp(headsetLocalSpeed.z, -1, 1), smoothing));

    }
}
