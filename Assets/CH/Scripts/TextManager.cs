﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class TextManager : MonoBehaviour
{
    public Text test;
    public GameObject newPage;
    public GameObject page1;
    int lines = 0;
    void Start()
    {
        string path = Application.dataPath + @"\TEXT_File.txt";

        string[] textValue = File.ReadAllLines(path);


        if (textValue.Length > 0)
        {
            test.text = "";
            for (int i = 0; i < textValue.Length; i++)
            {
                if (lines > 11)
                {
                    MakePage();
                    lines = 0;
                }
                test.text += textValue[i] + "\n";
                //Debug.Log(textValue[i]);
                lines++;
            }
            for (int i = 1; i < test.transform.parent.parent.childCount; i++)
            {
                test.transform.parent.parent.GetChild(i).gameObject.SetActive(false);
            }
        }
    }
    void MakePage()
    {
        GameObject page = Instantiate<GameObject>(newPage);
        page.transform.parent = test.transform.parent.parent;
        page.transform.position = page1.transform.position;
        //page.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        page.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
        page.transform.localScale = Vector3.one;
        test = page.transform.GetChild(1).GetComponent<Text>();
        test.text = "";
        //page.SetActive(false);
    }
}
