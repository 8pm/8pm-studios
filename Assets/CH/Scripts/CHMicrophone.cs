﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Voice.Unity;

[RequireComponent(typeof(AudioSource))]
public class CHMicrophone : MonoBehaviour
{
    
    AudioSource audio;
    Recorder recorder;
    void Start()
    {
        audio = GetComponent<AudioSource>();
        recorder = GetComponent<Recorder>();
        //audio.clip = Microphone.Start(Microphone.devices[0].ToString(), true, 10, 44100);
        audio.clip = recorder.AudioClip;
        audio.loop = true;
        while (!(Microphone.GetPosition(null) > 0)) { }
        audio.Play();


        
    }

    public  void PlaySound()
    {
        audio.Play();
        
    }


    void Update()
    {
        
    }
}
