﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Networking.Pun2;

public class NetworkPlayer : MonoBehaviourPun
{
    PhotonView pv;
    private void Start()
    {
        pv = GetComponent<PhotonView>();
        if (pv.IsMine)
        {
            GameObject ikET = transform.Find("IKEyeTarget (1)").transform.gameObject;
            GameObject ikLT = transform.Find("IKLeftTarget (1)").transform.gameObject;
            GameObject ikRT = transform.Find("IKRightTarget (1)").transform.gameObject;

            pv.ObservedComponents.Add(ikET.GetComponent<PhotonTransformView>());
            pv.ObservedComponents.Add(ikLT.GetComponent<PhotonTransformView>());
            pv.ObservedComponents.Add(ikRT.GetComponent<PhotonTransformView>());

            ikET.transform.parent = ChOculusPlayer.instance.head.transform.parent;
            ikLT.transform.parent = ChOculusPlayer.instance.leftHand.transform.parent;
            ikRT.transform.parent = ChOculusPlayer.instance.rightHand.transform.parent;

            ikET.transform.position = ChOculusPlayer.instance.head.transform.position;
            ikLT.transform.position = ChOculusPlayer.instance.leftHand.transform.position;
            ikRT.transform.position = ChOculusPlayer.instance.rightHand.transform.position;
        }
    }
    private void LateUpdate()
    {
    }
}
