﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RideCar : MonoBehaviour
{
    bool canRide;
    void Update()
    {
        if (canRide && OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.RTouch))
        {
            transform.parent.GetComponent<BNG.BNGPlayerController>().RidingCar();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Contains("inoutCol"))
        {
            canRide = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        canRide = false;
    }
}
