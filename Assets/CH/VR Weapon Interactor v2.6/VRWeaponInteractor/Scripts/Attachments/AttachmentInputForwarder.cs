﻿//========= Copyright 2018, Sam Tague, All rights reserved. ===================
//
// This Script should be attached to the gunhandler object and will forward select input actions
// onto any attachment that is currently attached to this gunhandler.
//
//=============================================================================
#if VRInteraction
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRInteraction;

namespace VRWeaponInteractor
{
	public class AttachmentInputForwarder : MonoBehaviour 
	{
		VRGunHandler gunHandler;

		void Start()
		{
			gunHandler = GetComponent<VRGunHandler>();
			if (gunHandler == null)
			{
				Debug.LogError("No VRGunHandler script attached to object", gameObject);
			}
		}

		virtual protected void ForwardMessage(VRInteractor hand, string message)
		{
			foreach(VRGunHandler.AttachmentPrefabs attachmentprefab in gunHandler.attachmentPrefabs)
			{
				if (attachmentprefab.attachmentReceiver == null || attachmentprefab.attachmentReceiver.currentAttachment == null) continue;
				attachmentprefab.attachmentReceiver.currentAttachment.gameObject.SendMessage(message, hand, SendMessageOptions.DontRequireReceiver);
			}
		}

		virtual protected void Eject(VRInteractor hand){ForwardMessage(hand, "Eject");}
		virtual protected void EjectReleased(VRInteractor hand){ForwardMessage(hand, "EjectReleased");}
		virtual protected void SlideRelease(VRInteractor hand){ForwardMessage(hand, "SlideRelease");}
		virtual protected void SlideReleaseReleased(VRInteractor hand){ForwardMessage(hand, "SlideReleaseReleased");}
		virtual protected void LightToggle(VRInteractor hand){ForwardMessage(hand, "LightToggle");}
		virtual protected void LightToggleReleased(VRInteractor hand){ForwardMessage(hand, "LightToggleReleased");}
		virtual protected void ScopeToggle(VRInteractor hand){ForwardMessage(hand, "ScopeToggle");}
		virtual protected void ScopeToggleReleased(VRInteractor hand){ForwardMessage(hand, "ScopeToggleReleased");}
	}
}
#endif