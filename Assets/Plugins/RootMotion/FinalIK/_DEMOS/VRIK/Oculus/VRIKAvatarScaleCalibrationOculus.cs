﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;
using Photon.Pun;

namespace RootMotion.Demos
{
    // Simple avatar scale calibration.
    public class VRIKAvatarScaleCalibrationOculus : MonoBehaviour
    {
        public VRIK ik;
        public float scaleMlp = 1f;
        bool oneShot = true;
        private void LateUpdate()
        {
            if (GetComponent<PhotonView>())
            {
                if (!GetComponent<PhotonView>().IsMine)
                {
                    enabled = false;
                    return;
                }
                if (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.RTouch) && oneShot)
                {
                    // Compare the height of the head target to the height of the head bone, multiply scale by that value.
                    float sizeF = (ik.solver.spine.headTarget.position.y - ik.references.root.position.y) / (ik.references.head.position.y - ik.references.root.position.y);
                    ik.references.root.localScale *= sizeF * scaleMlp;
                    oneShot = false;
                }
            }
        }

    }
}
