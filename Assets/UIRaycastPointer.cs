﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIRaycastPointer : MonoBehaviour
{
    public float defaultLength = 10.0f;
    LineRenderer lr;

    public EventSystem eventSystem;
    public VRInput myModule;
    // Start is called before the first frame update
    void Awake()
    {
        lr = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        lr.SetPosition(0, transform.position);
        lr.SetPosition(1, GetEnd());
    }

    public Vector3 GetEnd()
    {
        PointerEventData eventData = new PointerEventData(eventSystem);
        eventData.position = myModule.mousePosition;

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, results);

        RaycastResult closest = new RaycastResult();
        foreach(RaycastResult r in results)
        {
            if(r.gameObject == null)
            {
                continue;
            }

            closest = r;
            break;
        }

        if(closest.distance == 0)
        {
            return CalculateEnd(defaultLength);
        }

        return CalculateEnd(closest.distance);
    }

    Vector3 CalculateEnd(float length)
    {
        return transform.position + transform.forward * length;
    }
}
