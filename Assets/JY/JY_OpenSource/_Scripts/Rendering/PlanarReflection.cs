﻿namespace Custom.Rendering
{
    using UnityEngine;

    [ExecuteAlways]
    public class PlanarReflection : MonoBehaviour
    {
        // referenses
        public Camera mainCamera;
        public Camera reflectionCamera;
        public Transform reflectionPlane;
        // jy 주석
        public RenderTexture outputTexture;
        // jy 추가
        //public Renderer outputRenderer;

        // parameters
        public bool copyCameraParamerers; // = true;
        public float verticalOffset;
        private bool isReady;

        // cache
        private Transform mainCamTransform;
        private Transform reflectionCamTransform;

        //HW도움
        public Camera[] Cameras;
        public Transform cameraRoot;

        //private void Start()
        //{
        //    copyCameraParamerers = false;

        //}
        private void Update()
        {
            if (isReady)
                RenderReflection();

            //HW도움
            //Cameras = cameraRoot.GetComponentsInChildren<Camera>();
            //foreach (var cam in Cameras)
            //{
            //    cam.stereoTargetEye = StereoTargetEyeMask.None;
            //}
        }

        private void RenderReflection()
        {
            // take main camera directions and position world space
            Vector3 cameraDirectionWorldSpace = mainCamTransform.forward;
            Vector3 cameraUpWorldSpace = mainCamTransform.up;
            Vector3 cameraPositionWorldSpace = mainCamTransform.position;

            cameraPositionWorldSpace.y += verticalOffset;

            // transform direction and position by reflection plane
            Vector3 cameraDirectionPlaneSpace = reflectionPlane.InverseTransformDirection(cameraDirectionWorldSpace);
            Vector3 cameraUpPlaneSpace = reflectionPlane.InverseTransformDirection(cameraUpWorldSpace);
            Vector3 cameraPositionPlaneSpace = reflectionPlane.InverseTransformPoint(cameraPositionWorldSpace);

            // invert direction and position by reflection plane
            cameraDirectionPlaneSpace.y *= -1;
            cameraUpPlaneSpace.y *= -1;
            cameraPositionPlaneSpace.y *= -1;

            // transform direction and position from reflection plane local space to world space
            cameraDirectionWorldSpace = reflectionPlane.TransformDirection(cameraDirectionPlaneSpace);
            cameraUpWorldSpace = reflectionPlane.TransformDirection(cameraUpPlaneSpace);
            cameraPositionWorldSpace = reflectionPlane.TransformPoint(cameraPositionPlaneSpace);

            // apply direction and position to reflection camera
            reflectionCamTransform.position = cameraPositionWorldSpace;
            reflectionCamTransform.LookAt(cameraPositionWorldSpace + cameraDirectionWorldSpace, cameraUpWorldSpace);
        }

        private void Start()
       // private void OnValidate()
        {
            if (mainCamera != null)
            {
                mainCamTransform = mainCamera.transform;
                isReady = true;
            }
            else
                isReady = false;

            if (reflectionCamera != null)
            {
                reflectionCamTransform = reflectionCamera.transform;
                isReady = true;
            }
            else
                isReady = false;

            if(isReady && copyCameraParamerers)
            {
                copyCameraParamerers = !copyCameraParamerers;
                reflectionCamera.CopyFrom(mainCamera);

                //jy 주석
                reflectionCamera.targetTexture = outputTexture;
                //jy 추가
                //reflectionCamera.targetTexture = GameObject.Find("GroundPlane _Test").GetComponent<Renderer>().material.mainTexture;
                
            }
                print(isReady +","+ copyCameraParamerers);
        }
    }
}