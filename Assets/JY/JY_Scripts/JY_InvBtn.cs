﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Photon.Pun;
using Photon.Realtime;

public class InvBtn : MonoBehaviour
{
    public GameObject inventory;
    [SerializeField]
    GameObject player;
    [SerializeField]
    GameObject lha;
    [SerializeField]
    GameObject rha;
    [SerializeField]
    GameObject vlg;
    [SerializeField]
    GameObject vrg;

    public bool isInvOpen;
    public Transform cameraRig;
    public float distanceToCamera;
    public JY_DataObjLink data;
    public Transform vrInvLayout;

    PhotonView pv;

    private void OnEnable()
    {
        lha = GameObject.Find("LeftHandAnchor");
        rha = GameObject.Find("RightHandAnchor");
        vlg = GameObject.Find("Vrg Left Grabber");
        vrg = GameObject.Find("Vrg Right Grabber");
        cameraRig = GameObject.Find("CenterEyeAnchor").transform;
        pv = GetComponent<PhotonView>();
    }

    private void Update()
    {
        if (OVRInput.GetDown(OVRInput.RawButton.X))
        {
            if (pv.IsMine == true)
            {
                pv.RPC("OpenAndCloseInv", RpcTarget.AllBuffered);
            }
        }
    }

    [PunRPC]
    public void OpenAndCloseInv()
    {
        if (isInvOpen == true)
        {
            inventory.SetActive(false);
            InventoryEscape();
            isInvOpen = false;
        }
        else
        {
            inventory.SetActive(true);
            InitSetting();
            isInvOpen = true;
            if (pv.IsMine == true)
            {
                InvInFrontOfPlayer();
            }
            foreach (GameObject item in data.insDis)
            {
                item.GetComponent<InsGamePrefab>().SetHoverPosAtInit();
            }
        }
    }

    public void InitSetting()
    {
        vlg.SetActive(false);
        vrg.SetActive(false);
        lha.GetComponent<JY_Controller_0123>().enabled = true;
        rha.GetComponent<JY_Controller_0123>().enabled = true;

        //메인탭들과 메인탭의 첫 번째 서브탭의 아이텝들을 UI에 뿌리기
        data.SetTabNames();
        if (data.insDis.Count == 0)
        {
            for (int i = 0; i < 12; i++)
            {
                string itemName = data.invAllData.mainList[0].subList[0].items[i];
                GameObject disPrefab = Instantiate(Resources.Load(itemName) as GameObject, vrInvLayout.GetChild(i).GetChild(0));
                disPrefab.transform.localPosition = new Vector3(0, 0, 0);
                data.insDis.Add(disPrefab);
            }
            data.isItemShown = true;
            data.depth.Add(0);
        }
        else
        {
            data.depth.Clear();
            data.depth.Add(0);
        }

    }

    public void InventoryEscape()
    {
        vlg.SetActive(true);
        vrg.SetActive(true);
        lha.GetComponent<JY_Controller_0123>().enabled = false;
        rha.GetComponent<JY_Controller_0123>().enabled = false;
        foreach (GameObject btn in data.mainTabs.Keys)
        {
            Destroy(btn);
        }
        foreach (GameObject btn in data.subTabs.Keys)
        {
            Destroy(btn);
        }

        data.isMainBtnsSet = false;
    }
    public void InvInFrontOfPlayer()
    {
        inventory.transform.position = cameraRig.position + Camera.main.transform.forward * distanceToCamera;
        inventory.transform.LookAt(cameraRig);
        inventory.transform.Rotate(0f, 180f, 0f);
    }
}
