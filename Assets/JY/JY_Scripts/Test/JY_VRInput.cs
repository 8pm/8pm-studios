﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//기본 입력처리를 재정의하는 클래스
public class JY_VRInput : BaseInput
{
    public OVRInput.Controller useController;

    void Start()
    {
        //Standalone Input Module이 사용할 기본 입력처리를 JY_VRInput으로 함
        GetComponent<BaseInputModule>().inputOverride = this;
    }

    public override bool GetMouseButtonDown(int button)
    {
        //return Input.GetButtonDown("Fire1");
        return OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, useController);
    }

    public override bool GetMouseButtonUp(int button)
    {
        //return Input.GetButtonUp("Fire1");
        return OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger);
    }

    public Camera eventCamera;
    public override Vector2 mousePosition
    {
        get
        {
            return new Vector2(eventCamera.pixelWidth / 2, eventCamera.pixelHeight / 2);
        }
    }

}
