﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JY_TestRotate : MonoBehaviour
{
    public float rotSpeed = 200;
    Vector3 angles;
    // Start is called before the first frame update
    void Start()
    {
        angles = transform.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        float x = -Input.GetAxis("Mouse Y");
        float y = Input.GetAxis("Mouse X");

        angles.x += x * rotSpeed * Time.deltaTime;
        angles.y += y * rotSpeed * Time.deltaTime;

        transform.eulerAngles = angles;
    }
}
