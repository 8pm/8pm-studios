﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JY_Raycast : MonoBehaviour
{
    public float rayLength = 10;
    public JY_VRInput vrInput;
    LineRenderer lr;
    EventSystem eventSystem;
    
    // Start is called before the first frame update
    void Start()
    {
        lr = GetComponent<LineRenderer>();
        lr.startWidth = lr.endWidth = 0.2f;
        eventSystem = EventSystem.current;
        eventSystem.SetSelectedGameObject(gameObject);
        lr.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(GameObject.Find("InvOnOff").GetComponent<InvBtn>().isInvOpen==true)
        {
            print("인벤토리 열림");
            lr.enabled = true;
        lr.SetPosition(0, transform.position);
        lr.SetPosition(1, GetEnd());
        }
        print("인벤토리 열리지 않음");
    }

    Vector3 GetEnd()
    {
        PointerEventData data = new PointerEventData(eventSystem);
        data.position = vrInput.mousePosition;

        List<RaycastResult> results = new List<RaycastResult>();

        eventSystem.RaycastAll(data, results);

        RaycastResult closest = new RaycastResult();

        foreach (RaycastResult r in results)
        {
            closest = r;
            break;
        }

        if(closest.distance == 0)
        {
            return CalcuateEnd(rayLength);
        }

        return CalcuateEnd(closest.distance);

    }

    Vector3 CalcuateEnd(float distance)
    {
        return transform.position + transform.forward * distance;
    }
}
