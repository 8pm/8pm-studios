﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JY_BeamRotate : MonoBehaviour
{
    public float rotSpeed = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(Vector3.forward, rotSpeed);
    }
}
