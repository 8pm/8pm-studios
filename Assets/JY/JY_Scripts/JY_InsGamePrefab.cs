﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Epitybe.VRInventory;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Photon.Pun;

//Display Prefab이 레이를 맞으면
//Display Prefab 앞 일정한 위치에
//대응하는 Game Prefab이 나타나 호버링
//속성: 호버링 지점, (GetInventoryItem통해 부르는) Game Prefab의 게임오브젝트, 회전 속도

public class InsGamePrefab : MonoBehaviour
{
    public float hoverDist = 0.05f;
    Vector3 hoverPos;
    private InventoryItem invItem;
    GameObject gamePrefab;
    public UnityEvent stopRot;

    // Start is gamePrefab before the first frame update
    void Start()
    {
        SetHoverPosAtInit();
    }

    public void SetHoverPosAtInit()
    {
        hoverPos = this.gameObject.transform.position - this.transform.forward * hoverDist;
    }


    public void LoadGamePrefab()
    {
        string name = gameObject.name;
        name = name.Substring(0, name.Length - 10);
        gamePrefab = PhotonNetwork.Instantiate("JY_Game/" + name + "Game", hoverPos, Quaternion.identity);
    }

    public void DestroyGamePrefab()
    {
        PhotonNetwork.Destroy(gamePrefab);
    }
    
    public void LoadLocaPrefab()
    {
        string name = gameObject.name;
        name = name.Substring(0, name.Length - 10);
        gamePrefab = PhotonNetwork.Instantiate("JY_Loca/" +name + "Loca", Vector3.zero, Quaternion.identity);
        gamePrefab.transform.position = Vector3.forward * 2f;
    }

    public void LoadColorPrefab()
    {
        string name = gameObject.name;
        name = name.Substring(0, name.Length - 10);
        gamePrefab = PhotonNetwork.Instantiate("JY_Color/" + name + "Color", hoverPos, Quaternion.identity);
    }
}
