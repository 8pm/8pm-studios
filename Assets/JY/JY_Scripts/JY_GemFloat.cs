﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JY_GemFloat : MonoBehaviour
{
    float currentTime;
    public float frequency= 5f;
    public float amplitude= 1f;

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        this.transform.position += new Vector3(0, Mathf.Sin(currentTime * frequency) * amplitude, 0);
    }
}
