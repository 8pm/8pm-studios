﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class JY_DataObjLink : MonoBehaviour
{
    InventoryData invData = new InventoryData();
    public InvAllData invAllData = new InvAllData();

    public GameObject mainTabBtn;
    public Transform mainTabLayoout;
    public GameObject subTabBtn;
    public Transform subTabLayout;

    public Dictionary<GameObject, Main> mainTabs = new Dictionary<GameObject, Main>();
    public Dictionary<GameObject, Sub> subTabs = new Dictionary<GameObject, Sub>();
    public List<GameObject> insDis = new List<GameObject>();

    public List<int> depth = new List<int>();
    public bool isMainBtnsSet;

    public bool isItemShown;

    private void Awake()
    {
        string inventoryData = invData.LoadData("InventoryData.txt");
        invAllData = invData.ReadData(inventoryData);
    }

    //시작할 때
    //뎁스를 0으로 정함
    //메인탭들의 버튼을 만들고,
    //각 버튼에 Main 구조체 할당
    public void SetTabNames()
    {
        for (int i = 0; i < invAllData.mainList.Count; i++)
        {
            GameObject mainTab = Instantiate(mainTabBtn, mainTabLayoout);
            mainTab.GetComponent<JY_28_MainTab>().data = transform.parent.GetChild(0).GetComponent<InvBtn>().data;
            Text[] names = mainTab.GetComponentsInChildren<Text>();
            foreach (Text txt in names)
            {
                txt.text = invAllData.mainList[i].mainName;
            }
            mainTabs.Add(mainTab, invAllData.mainList[i]);
            isMainBtnsSet = true;
        }
    }
}
