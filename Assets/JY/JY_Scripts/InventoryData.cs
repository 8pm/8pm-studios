﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[Serializable]
public struct InvAllData //인벤토리 전체의 정보
{
    public int mainCount;
    public List<Main> mainList;
}

[Serializable]
public struct Main
{
    public string mainName;
    public int subCount;
    public List<Sub> subList;
}

[Serializable]
public struct Sub
{
    public string subName;
    public int itemCount;
    public List<string> items;
}

public class InventoryData
{
    public int numberofMainTabs;
    public List<string> mainTabNames = new List<string>();

    public List<int> depth = new List<int>();
    public List<GameObject> mainTabs = new List<GameObject>();

    //텍스트파일 불러오기
    public string LoadData(string filePath)
    {
        string path = Path.Combine(Application.streamingAssetsPath, filePath);
        FileInfo fileInfo = new FileInfo(path);
        string value = "";
        if (fileInfo.Exists)
        {
            StreamReader reader = new StreamReader(path);
            value = reader.ReadToEnd();
            reader.Close();
        }
        else
        {
            value = "파일이 없습니다.";
        }
        return value;
    }

    //텍스트파일의 내용 분리해서 읽기
    public InvAllData ReadData(string data)
    {
        InvAllData invAllData = new InvAllData();

        string[] dataLine = data.Split("\n".ToCharArray()); //파일 다 읽기
        int currentIndex = 0; // InventoryData 텍스트 파일의 첫째 행은 전체 메인의 갯수
        int mainCount = int.Parse(dataLine[currentIndex].Split(",".ToCharArray())[1].Trim());

        invAllData.mainCount = mainCount;
        currentIndex++;
        invAllData.mainList = new List<Main>();
        for (int i = 0; i < mainCount; i++)
        {
            Main main = new Main();
            main.mainName = dataLine[currentIndex].Split(",".ToCharArray())[1].Trim();
            currentIndex++;
            main.subCount = int.Parse(dataLine[currentIndex].Split(",".ToCharArray())[1].Trim());
            currentIndex++;
            main.subList = new List<Sub>();
            for (int j = 0; j < main.subCount; j++)
            {
                Sub sub = new Sub();
                sub.subName = dataLine[currentIndex].Split(",".ToCharArray())[1].Trim();
                currentIndex++;
                sub.itemCount = int.Parse(dataLine[currentIndex].Split(",".ToCharArray())[1].Trim());
                currentIndex++;
                sub.items = new List<string>();

                for (int k = 0; k < sub.itemCount; k++)
                {
                    string itemName = dataLine[currentIndex].Split(",".ToCharArray())[1].Trim();
                    currentIndex++;
                    sub.items.Add(itemName);
                }
                main.subList.Add(sub);
            }

            invAllData.mainList.Add(main);
        }
        Debug.Log("Test");
        return invAllData;
    }
}