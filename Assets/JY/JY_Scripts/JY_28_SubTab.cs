﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JY_28_SubTab : MonoBehaviour
{
    JY_DataObjLink data;
    Transform vrInvLayout;
    GameObject mainTab;
    Main main;
    Sub sub;
    public int depthAtItemShow;
    
    private void OnEnable()
    {
        data = GameObject.Find("InvData").GetComponent<JY_DataObjLink>();
        vrInvLayout = GameObject.Find("ItemsLayout").transform;
    }

    private void Start()
    {
        sub = data.subTabs[gameObject];

        foreach (Main m in data.invAllData.mainList)
        {
            if (m.subList.Contains(sub))
            {
                main = m;
            }
        }
    }

    //선택되는 서브탭 따라 depth 변경
    public void ChangeDepthSub()
    {
        if (data.depth.Count == 2) //-> 뎁스가 [1,n]일 때: 세 번째 원소를 추가
        {
            data.depth.Add(main.subList.IndexOf(sub));
        }

        else if (data.depth.Count == 3) //depth가 [1,n,n]일 때: 세 번째 요소를 변경
        {
            data.depth[2] = main.subList.IndexOf(sub);
        }
    }

    //뎁스에 해당하는 데이터들을 인벤토리 칸에 맞춰 나타나게 함
    public void ShowItems()
    {
        if (data.depth.Count == 3)
        {
            if (data.isItemShown == true)
            {
                DestroyItems();
                data.isItemShown = false;
            }
            for (int i = 0; i <12; i++)
            {
                string itemName = sub.items[i];
                GameObject disPrefab = Instantiate(Resources.Load(itemName) as GameObject, vrInvLayout.GetChild(i).GetChild(0));
                disPrefab.transform.localPosition = new Vector3(0, 0, 0);
                data.insDis.Add(disPrefab);
            }
            data.isItemShown = true;
        }
    }

    //기존에 보이던 아이템 모두 파괴
    public void DestroyItems()
    {
        foreach (GameObject go in data.insDis)
        {
            Destroy(go);
        }
    }
}