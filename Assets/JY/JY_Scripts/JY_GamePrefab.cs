﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JY_GamePrefab : MonoBehaviour
{
    public bool isSelected;
    public float rotSpeed = 35f;


    // Update is called once per frame
    void Update()
    {
        if(isSelected== false)
        {
            HoverRotate();
        }
    }

    public void GravityOn()
    {
        GetComponent<Rigidbody>().useGravity = true;
    }

    public void HoverRotate()
    {
        float currentTime = 0;
        currentTime += Time.deltaTime;
        transform.Rotate(Vector3.up, rotSpeed * currentTime);
    }
}
