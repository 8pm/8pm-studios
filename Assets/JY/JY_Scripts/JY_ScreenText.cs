﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// 텍스트들이 순서대로 또는 사용자 입력에 따라 페이드인, 페이드아웃 되게 함

public class JY_ScreenText : MonoBehaviour
{
    public Text greeting;
    public Text text;
    public Text searchiing;
    public Button[] roles;
    public Button[] genres;
    public float fadeOut = 0.3f;
    float currentTime;
    public bool genreSelected;

    public List<GameObject> texts = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject go in texts)
        {
            go.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < texts.Count; i++)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                texts[i].SetActive(true);
                text = texts[i].GetComponent<Text>();
                StartCoroutine(FadeTextToFullAlpha());
                StartCoroutine(Delay());
                texts[i].SetActive(false);
            }
        }
    }

    private IEnumerator Delay()
    {
        yield return new WaitWhile(() => Input.GetKeyDown(KeyCode.Alpha1));
    }

    public IEnumerator FadeTextToFullAlpha() // 알파값 0에서 1로 전환
    {
        text.color = new Color(text.color.r, text.color.g, text.color.b, 0);
        while (text.color.a < 1.0f)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a + (Time.deltaTime / 3.0f));
            yield return null;
        }
        //StartCoroutine(FadeTextToZeroAlpha());
    }

    public IEnumerator FadeTextToZeroAlpha()  // 알파값 1에서 0으로 전환
    {
        text.color = new Color(text.color.r, text.color.g, text.color.b, 1);
        while (text.color.a > 0.0f)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - (Time.deltaTime / 3.0f));
            yield return null;
        }
        StartCoroutine(FadeTextToFullAlpha());
    }
}

