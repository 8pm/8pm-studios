﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VrGrabber;

public class JY_Rotate : MonoBehaviour
{
    OVRPlayerController opc;
    public OVRInput.Controller useController;
    LineRenderer lr;
    GameObject grabObj;

    // Start is called before the first frame update
    void Start()
    {
        opc = GameObject.Find("OVRPlayerController").GetComponent<OVRPlayerController>();
        lr=GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (opc.EnableRotation == true)
            opc.EnableRotation = !opc.EnableRotation;

        //VrgGrabber로 돌아가기
        if(OVRInput.GetDown(OVRInput.RawButton.RHandTrigger))
        {
            this.gameObject.GetComponent<VrgGrabber>().enabled = true;
            grabObj = null;
            this.enabled = false;
        }
        Raycast();
    }

    public float speed = 2f;
    float angle;
    Vector3 pos;
    Ray ray;
    RaycastHit hit;

    void Raycast()
    {
        ray = GetRay();

        int layerMask = 1 << 14;

        if (Physics.Raycast(ray, out hit, 1000, layerMask))
        {
            DrawLine();
            GameObject go = hit.transform.gameObject;
            grabObj = go;
            Rotate2();
        }
    }

    void Rotate2()
    {
        if (grabObj)
        {
            angle = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.RTouch).x * speed;
            grabObj.transform.Rotate(Vector3.down, angle);
            print("x값은:" + OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.RTouch).x);
            print("앵글" + angle);
        }
    }

    void DrawLine()
    {
        GetComponent<LineRenderer>().enabled = true;
    }


    //Ray를 반환
    Ray GetRay()
    {
        //오른손 위치 찾아서 월드 좌표로 바꿔줌
        pos = OVRInput.GetLocalControllerPosition(useController);
        pos = GameObject.Find("TrackingSpace").transform.TransformPoint(pos);
        //오른손 방향
        Quaternion rotation = OVRInput.GetLocalControllerRotation(useController);
        Vector3 direction = transform.forward;

        Ray ray = new Ray(pos, direction);

        return ray;
    }
}
