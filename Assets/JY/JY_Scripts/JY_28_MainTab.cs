﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JY_28_MainTab : MonoBehaviour
{
    public GameObject obj;
    public JY_DataObjLink data;
    Transform SelectedMainTabPos;
    Transform mainTabsLayout;
    Vector3 originPos;
    int depth0Index;
    public bool isSubTabsSet;

    private void OnEnable()
    {
        originPos = gameObject.transform.position;
    }

    IEnumerator IndexDelay()
    {
        yield return new WaitWhile(() => data.isMainBtnsSet);
    }

    public void Start()
    {
        if(data.isMainBtnsSet!= true)
        {
        StartCoroutine(IndexDelay());
        }

        mainTabsLayout = data.transform.parent.GetChild(2).Find("mainTabsLayout").transform;

        SelectedMainTabPos = data.transform.parent.GetChild(2).Find("SelectedMainTabPos").transform;
        depth0Index = data.invAllData.mainList.IndexOf(data.mainTabs[gameObject]);
    }

    //현재 뎁스에 따라 클릭하면 뎁스를 변하게 하기
    public void ChangeDepth()
    {
        if (data.depth.Count == 1)
        {
            data.depth[0] = 1;
            Main main = data.mainTabs[gameObject];
            data.depth.Add(data.invAllData.mainList.IndexOf(main));
        }
        else if (data.depth.Count == 2)
        {
            data.depth[0] = 0;
            data.depth.RemoveAt(1);
        }
        else
        {
            data.depth[0] = 0;
            data.depth.RemoveAt(1);
            data.depth.RemoveAt(1);
        }

    }

    //depth의 길이가 1일 때: 자신을 제외한 메인탭 버튼들이 사라지고 자신의 서브탭 버튼들이 생성됨
    //depth의 길이가 1보다 클 때: 버튼이 모두 사라지고 메인탭 버튼들이 모두 생성됨
    public void FlipMainAndItsSubs()
    {
        if (data.depth.Count==2 && data.depth[0] == 1) //depth가 [1,n]일 때
        {
            foreach (GameObject mainTab in data.mainTabs.Keys) //선택된 메인탭 외의 메인탭들은 사라짐
            {
                if (mainTab != this.gameObject)
                {
                    Destroy(mainTab);
                }
            }
            CreateSubTab(); //선택된 메인탭의 서브탭들이 나타남
        }
        
        else if(data.depth.Count==1 || data.depth.Count==3) //depth가 [0]이거나 [1,n,n]일 때, 메인탭과 서브탭이 모두 사라짐
        {
            Destroy(this.gameObject); //자신의 버튼
            Main main= data.mainTabs[gameObject];
            for (int i = 0; i < data.mainTabs[gameObject].subCount; i++)
            {
                foreach(GameObject btn in data.subTabs.Keys)
                {
                    Destroy(btn);
                }
                isSubTabsSet = false;
            }
            
            for (int i = 0; i < data.invAllData.mainCount; i++) //메인탭들이 모두 나타남
            {
                GameObject mainTab = Instantiate(data.mainTabBtn, data.mainTabLayoout);
                mainTab.GetComponent<JY_28_MainTab>().data = data;
                Text[] names = mainTab.GetComponentsInChildren<Text>();
                foreach (Text txt in names)
                {
                    txt.text = data.invAllData.mainList[i].mainName;
                }
                data.mainTabs.Add(mainTab, data.invAllData.mainList[i]);
            }
        }
    }

    //depth가 [1,n]일 때 서브탭 버튼들이 나타남
    public void CreateSubTab()
    {
            Main main = data.mainTabs[gameObject]; //해당 오브젝트를 key로 갖는 value인 Main 구조체에서
            for (int i = 0; i < main.subCount; i++)
            {
                GameObject subTab = Instantiate(data.subTabBtn, data.subTabLayout); //subCount를 찾아 그 값만큼 버튼을 생성하고
                Text[] names = subTab.GetComponentsInChildren<Text>();
                foreach (Text txt in names)
                {
                    txt.text = main.subList[i].subName; //이름을 붙여준 후
                }
                data.subTabs.Add(subTab, main.subList[i]); //버튼 오브젝트와 Sub 구조체를 딕셔너리로 묶어줌
            }
            isSubTabsSet = true;
    }

    //depth가 [1,n]일 때 위치
    public void MoveToDepth1Position()
    {
        if (data.depth.Count == 2)
        {
            transform.SetParent(SelectedMainTabPos,false);
            transform.localPosition = Vector3.zero;
        }
    }

    //depth가 [0]일 때 위치
    public void BackToDepth0Position()
    {
        if (data.depth.Count == 1)
        {
            transform.SetParent(mainTabsLayout, false);
            transform.SetSiblingIndex(depth0Index);
        }
    }
}

