﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//로케의 Game Item을 선택한 상태에서 IndexTrigger를 누르면 Game Item이 사라지고 로케가 로드되며 배경이 바뀜
//다른 로케 Game Item에서 반복하면 원래 로케가 사라지고 새 로케가 로드됨
// 이 스크립트는 각 로케 Game Item에 붙이고, 로드될 로케 오브젝트의 이름은 Game Item에 일정한 string을 더해 지어서 쉽게 불러옴

public class JY_LocaChange : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;

    GameObject prevLoca;
    GameObject newLoca;
    public Transform locPos;

    // Update is called once per frame
    void Update()
    {
        //레이를 맞은 것이 Game Item이라면,
        if (Input.GetMouseButtonDown(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform == this.transform)
                {
                        LoadLoca();
                }
            }
        }
    }
   
    public void LoadLoca()
    {
        if (prevLoca != null)
        {
            Destroy(prevLoca);
            Resources.UnloadUnusedAssets();
        }

        string name = this.gameObject.name;
        name = name.Substring(0, name.Length - 4);
        newLoca = Instantiate(Resources.Load("JY_Loca/" + name + "Loca")) as GameObject;
        newLoca.transform.position = locPos.position;
        prevLoca = newLoca;
    }
}


