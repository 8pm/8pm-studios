﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JY_GemManager : MonoBehaviour
{
    public List<GameObject> gems;

    public JY_ScreenText st;
    public float rotSpeed;

    // Start is called before the first frame update
    void Start()
    {
        foreach(GameObject go in gems)
        {
            go.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(st.genreSelected==true)
        {
            for(int i=0; i<gems.Count; i++)
            {
                gems[i].SetActive(true);
                GemDelay();
            }
        }

        gems[1].transform.Rotate(Vector3.up, rotSpeed);
        gems[2].transform.Rotate(Vector3.forward, rotSpeed);
        gems[3].transform.Rotate(Vector3.up, rotSpeed);
        gems[4].transform.Rotate(Vector3.forward, rotSpeed);
        gems[5].transform.Rotate(Vector3.up, rotSpeed);
    }

    private IEnumerator GemDelay()
    {
        yield return new WaitForSeconds(0.4f);
    }
}
