﻿//#define PC
#define OCULUS

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Epitybe.VRInventory;
using System;
using UnityEngine.EventSystems;
using Photon.Pun;

public class JY_Controller_0123 : MonoBehaviour
{
    //UI용 레이
    public float rayLength = 10;
    public JY_VRInput vrInput;
    LineRenderer lr;
    EventSystem eventSystem;

    //3D오브젝트용 레이
    Rigidbody rb;
    public float angularVel = 10;
    public float velDistance = 10;
    Ray ray;
    public RaycastHit hit;
    //LineRenderer lr;
    public Transform rayOrigin;
    Vector3 pos;

    //뎁스
    JY_DataObjLink invData;

    //아이템 배치, 크기조절, 회전에 사용할 그래버
    GameObject vrgLeftGrabber;
    GameObject vrgRightGrabber;

    //인벤토리를 끄고 켜기
    InvBtn jyInvOnOff;

    //UI에 맞았는지 안 맞았는지 확인
    public bool isUIHit;
    public Vector3 endPoint;

    //플레이어 앞에 나타나도록
    public Transform cameraRig;
    public float gameDist;
    public GameObject ownInv;

    InsGamePrefab dcg = null;

    [SerializeField]
    Transform prevBtn;
    [SerializeField]
    Transform nextBtn;

    public float hoverDist = 1.5f;

    // 사용할 컨트롤러
    public OVRInput.Controller useController;

    public GameObject floor;


    // Start is gamePrefab before the first frame update
    void Start()
    {
        vrgLeftGrabber = GameObject.Find("Vrg Left Grabber");
        vrgRightGrabber = GameObject.Find("Vrg Right Grabber");
        if (GetComponent<Rigidbody>() != null)
        {
            rb = GetComponent<Rigidbody>();
        }
        lr = GetComponent<LineRenderer>();
        eventSystem = GameObject.Find("JY_Event System").GetComponent<EventSystem>();
        eventSystem.SetSelectedGameObject(null);
        lr.enabled = false;
        vrInput = eventSystem.GetComponent<JY_VRInput>();
    }

    //스크립트가 꺼지면 라인도 꺼짐(그래버 바꿀 때 겹치지 않도록)
    private void OnDisable()
    {
        lr.enabled = false;
    }

    // Update is gamePrefab once per frame
    void Update()
    {
#if PC
        if (Input.GetMouseButton(0))
            RayCast();
#elif OCULUS

       
        if (jyInvOnOff == null)
        {
            jyInvOnOff= ownInv.transform.GetChild(0).GetComponent<InvBtn>();
        }
        else
        {
            if (jyInvOnOff.isInvOpen == true) //인벤토리가 켜져있는 동안 계속
            {
                //레이 종류와 상관없이 시작점은 항상 손
                pos = OVRInput.GetLocalControllerPosition(useController);
                pos = GameObject.Find("TrackingSpace").transform.TransformPoint(pos);
 
                if (lr.enabled == false)
                {
                    lr.enabled = true;
                }

                // UI 충돌 체크
                GetEnd();
                endPoint = GetEnd();

                // 물체 충돌 체크
                RayCast();

                // 체크한 결과에 따라 라인 그리기 
                lr.SetPosition(0, pos);
                lr.SetPosition(1, endPoint);
              
            }
            else //인벤토리 꺼지면 라인 그려지지 않음
            {
                lr.enabled = false;
            }
        }
        if (invData == null)
        {
            invData = ownInv.transform.GetChild(1).GetComponent<JY_DataObjLink>();
        }
#endif
    }

    // 아이템을 그랩한 동안에는 플레이어가 RTouch로 이동하지 않도록 함
    public void DisableMove()
    {
        OVRPlayerController opc = GameObject.Find("OVRPlayerController").GetComponent<OVRPlayerController>();
        opc.enabled = false;
    }


    public void RayCast()
    {
#if PC
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            //다른 오브젝트로 바꾸면 다시 게임프리팹 불러오게 하기
            if (hit.transform.gameObject.layer == 13&& isgamePrefab==false)
            {
                candidate = hit.transform.gameObject;

              //  if()
                print("controller레이에 맞은 건"+hit.transform.gameObject.name);
                hover= hit.transform.gameObject.GetComponent<JY_GetInventoryItem>().invItem;
                //hover.displayPrefab = hit.transform.gameObject;
                // 게임아이템을 호버링시킨다
                HoverDisplay();
                prevCandidate = hit.transform.gameObject;
            }
            if (hit.transform.gameObject.layer == 14)
            {
                // 배치(이동, 회전), 삭제는 다른 버튼으로
                print("배치(이동, 회전), 삭제는 다른 버튼으로");
            }
        }
#elif OCULUS

        ray = GetRay();
        Debug.DrawRay(ray.origin, ray.direction, Color.red);
        int layerMask = 1<<11 | 1 << 12 | 1 << 13 | 1 << 14;

        if (Physics.Raycast(ray, out hit, 1000, layerMask))
        {
            LoadGamePrefab();
            SwitchGrabber();
            FixLoca();
            FixColor();
            endPoint = hit.point;
        }
        else
        {

            if (dcg != null)
            {
                dcg.DestroyGamePrefab();
                dcg = null;
            }
        }
#endif
    }

    List<GameObject> insList = new List<GameObject>();
    //장소가 아닌 Game Prefab을 선택했다면 vrg 그래버로 컨트롤러 전환
    private void SwitchGrabber()
    {
        if (jyInvOnOff.isInvOpen == false)
        {
            return;
        }
        else
        {
            if ((invData.depth[0] == 0 || invData.depth[1] != 0) && hit.transform.gameObject.layer == 14)
            {
                if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, useController))
                {
                    //오브젝트 가까이 가져오고(Lerp사용하는 걸로 바꾸기)
                    hit.transform.position = cameraRig.position + Camera.main.transform.forward * gameDist;
                    hit.transform.gameObject.GetComponent<JY_GamePrefab>().isSelected = true;
                    //클릭해서 인스턴스화된 게임프리팹은 다른 변수명을 지정해주거나 따로 리스트에 담아 관리
                    GameObject clicked = hit.transform.gameObject;
                    dcg = null;
                    insList.Add(clicked);
                    // 배치(이동, 회전), 삭제는 다른 컨트롤러로
                    vrgLeftGrabber.SetActive(true);
                    vrgRightGrabber.SetActive(true);
                    this.GetComponent<JY_Controller_0123>().enabled = false;
                }
            }
        }
    }

    //Display Prefab 선택해 Game Prefab 불러오기
    private void LoadGamePrefab()
    {
        if (hit.transform.gameObject.layer == 13 && dcg == null)
        {
            dcg = hit.transform.gameObject.GetComponent<InsGamePrefab>();
            dcg.LoadGamePrefab();
        }
        else if (hit.transform.gameObject.layer == 12 && dcg == null)
        {
            dcg = hit.transform.gameObject.GetComponent<InsGamePrefab>();
            dcg.LoadLocaPrefab();
        }
        else if (hit.transform.gameObject.layer == 11 && dcg == null)
        {
            dcg = hit.transform.gameObject.GetComponent<InsGamePrefab>();
            dcg.LoadColorPrefab();
        }

    }
    public bool isLocaFixed;
    public GameObject fixedLoca;

    //레이어 12에 맞은 상태에서 인덱스트리거 누르면 장소를 고정
    private void FixLoca()
    {
        if ((hit.transform.gameObject.layer == 12))
        {
            if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, useController))
            {
                if (fixedLoca != null)
                {
                    PhotonNetwork.Destroy(fixedLoca);
                }

                string name = hit.transform.gameObject.name;
                name = name.Substring(0, name.Length - 10);
                fixedLoca = PhotonNetwork.Instantiate("JY_Loca/" + name + "Loca", Vector3.zero, Quaternion.identity);
                fixedLoca.transform.position = Vector3.forward * 2f;
            }
        }
    }

    public bool isColFixed;
    public GameObject fixedCol;

    private void FixColor()
    {
        if ((hit.transform.gameObject.layer == 11))
        {
            if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, useController))
            {
                if (fixedCol != null)
                {
                    PhotonNetwork.Destroy(fixedCol);
                }

                string name = hit.transform.gameObject.name;
                name = name.Substring(0, name.Length - 10);
                fixedCol = PhotonNetwork.Instantiate("JY_Color/" + name + "Color", Vector3.zero, Quaternion.identity);
                fixedCol.transform.position = Vector3.forward * 2f;
            }
        }
    }

    //Ray를 반환
    Ray GetRay()
    {
        //오른손 위치 찾아서 월드 좌표로 바꿔줌
        pos = OVRInput.GetLocalControllerPosition(useController);
        pos = GameObject.Find("TrackingSpace").transform.TransformPoint(pos);
        //오른손 방향
        Vector3 direction = transform.forward;
        Ray ray = new Ray(pos, direction);
        return ray;
    }

    //레이를 그림
    void DrawLine()
    {
        //UI와 부딪힌 경우
        GetEnd();
        if (isUIHit == true)
        {
            lr.SetPosition(0, pos);
            lr.SetPosition(1, endPoint);
        }
        //물체와 부딪힌 경우
        else
        {
            lr.SetPosition(0, pos);
            lr.SetPosition(1, hit.point);
        }
        // 아무것도 안부딪힌 경우
    }
    
    //VR에서 UI를 검출하기 위한 레이를 쏠 때 부딪히는 지점과 끝나는 지점
    Vector3 GetEnd()
    {
        PointerEventData data = new PointerEventData(eventSystem);
        data.position = vrInput.mousePosition;
        List<RaycastResult> results = new List<RaycastResult>();
        eventSystem.RaycastAll(data, results);
        RaycastResult closest = new RaycastResult();
        foreach (RaycastResult r in results)
        {
            closest = r;
            isUIHit = true;
            Debug.Log("UI 맞음");
            break;
        }
        if (closest.distance == 0)
        {
            Debug.Log("UI 맞지 않음");
            return CalcuateEnd(rayLength);
        }
        return CalcuateEnd(closest.distance);
    }
    Vector3 CalcuateEnd(float distance)
    {
        isUIHit = false;
        return transform.position + transform.forward * distance;
    }
}

