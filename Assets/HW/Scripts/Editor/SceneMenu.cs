﻿using UnityEditor;
using UnityEditor.SceneManagement;

public static class SceneMenu
{
    private static string HW = "Assets/HW/Scenes/Camera/";

    [MenuItem("Scenes/HW/Camera")]
    public static void OpenScene()
    {
        OpenSceneByName("Camera");
    }

    private static void OpenSceneByName(string sceneName)
    {
        EditorSceneManager.OpenScene(HW + sceneName + ".unity", OpenSceneMode.Single);
    }
}
