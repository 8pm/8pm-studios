﻿using UnityEngine;

public class PathPointDetector : MonoBehaviour
{
    public delegate void Detect(Transform detectedPoint);
    public Detect PathPointDetectAction;

    private bool _buttonPressed = false;
    private LayerMask _pathPointMask = 1 << 23;

    public bool IsTriggerStay { get; private set; } = false;

    private void OnTriggerStay(Collider other)
    {
        if ((_pathPointMask & (1 << other.gameObject.layer)) != 0)
        {
            IsTriggerStay = true;
            if (_buttonPressed)
            {
                PathPointDetectAction?.Invoke(other.transform);
            }
        }
        else
        {
            IsTriggerStay = false;
        }
    }

    #region Callbacks
    /// <summary>
    /// Called on VR Button OnButtonDown Event.
    /// </summary>
    public void OnButtonDownAction()
    {
        if (IsTriggerStay)
        {
            _buttonPressed = true;
        }
    }

    /// <summary>
    /// Called on VR Button OnButtonUp Event.
    /// </summary>
    public void OnButtonUpAction()
    {
        _buttonPressed = false;
    }
    #endregion
}