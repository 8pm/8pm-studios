﻿using System.Collections.Generic;
using UnityEngine;

public class PathPool
{
    #region Private Field
    private MonoBehaviour _mono = null;
    private Stack<GameObject> _pathPool = null;
    private GameObject _spawnObject = null;
    private GameObject _poolRoot = null;
    private int _pathCount = 10;
    #endregion

    public PathPool(MonoBehaviour mono, GameObject spawnObject, int pathCount)
    {
        InitVariables(mono, spawnObject, pathCount);
        CreatePool();
    }

    #region Initialize
    private void InitVariables(MonoBehaviour mono, GameObject obj, int count)
    {
        _mono = mono;
        _spawnObject = obj;
        _pathCount = count;
        _pathPool = new Stack<GameObject>();
    }

    private void CreatePool()
    {
        _poolRoot = GameObject.Find("PathPool");
        if (_poolRoot == null)
        {
            _poolRoot = new GameObject("PathPool");
        }
        for (int i = 0; i < _pathCount; i++)
        {
            var point = MonoBehaviour.Instantiate(_spawnObject, _mono.transform.position,
                Quaternion.identity, _poolRoot.transform);
            point.SetActive(false);
            _pathPool.Push(point);
        }
    }
    #endregion

    #region Control Functions
    /// <summary>
    /// Activates one Path Point from the pool and return it.
    /// </summary>
    /// <param name="spawnPoint"></param>
    /// <returns></returns>
    public Transform ActivatePoint(Vector3 spawnPoint, Quaternion spawnRotation)
    {
        if (_pathPool.Count < 1) return null;
        var point = _pathPool.Pop();
        point.transform.position = spawnPoint;
        point.transform.rotation = spawnRotation;
        point.SetActive(true);
        return point.transform;
    }

    /// <summary>
    /// Terminates the given Path Point and push it back to the pool.
    /// </summary>
    /// <param name="livePoint"></param>
    public void TerminatePoint(Transform livePoint)
    {
        var point = livePoint.gameObject;
        point.transform.position = _mono.transform.position;
        point.transform.rotation = Quaternion.identity;
        point.SetActive(false);
        _pathPool.Push(point);
    }
    #endregion
}
