﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartController : MonoBehaviour
{
    private bool _isCoroutineRunning = false;

    #region
    public void SetPosition(Vector3 pos)
    {
        transform.position = pos;
    }
    #endregion
}
