﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathCreator : MonoBehaviour
{
    #region Serialized Field
    [Header("Controls")]
    [SerializeField]
    private GameObject cartPrefab = null;
    [SerializeField]
    private Transform inputs = null;
    [SerializeField]
    private Transform spawnPoint = null;
    [Header("Path Point")]
    [SerializeField]
    private GameObject spawnObject = null;
    [SerializeField]
    private int spawnCount = 10;
    [SerializeField]
    private Material anchorPointMat = null;
    [SerializeField]
    private Material controlPointMat = null;
    [Header("Path Line")]
    [SerializeField]
    private Material pathMat = null;
    [SerializeField]
    private Material controlPathMat = null;
    #endregion

    #region Private Field
    private PathPool _pool = null;
    private PathViewer _viewer = null;
    private PathPointDetector _detector = null;
    private CartController _cartController = null;

    private List<Transform> _points = new List<Transform>();
    private GameObject _cart = null;
    private bool _isCartMoving = false;
    private bool isCor = false;
    #endregion

    private void Awake()
    {
        InitVariables();
        IgnoreCollisions();
    }

    private void OnEnable()
    {
        _detector.PathPointDetectAction += RemovePoint;
    }

    #region Initialize
    private void InitVariables()
    {
        _pool = new PathPool(this, spawnObject, spawnCount);
        _viewer = new PathViewer(_points, pathMat, controlPathMat, anchorPointMat, controlPointMat);
        _detector = spawnPoint.GetComponent<PathPointDetector>();
        _cart = Instantiate(cartPrefab, transform.position, Quaternion.identity);
        _cartController = _cart.GetComponent<CartController>();
        _cart.SetActive(false);
    }

    private void IgnoreCollisions()
    {
        Collider creatorCollider = transform.GetComponent<Collider>();
        Collider[] colliders = inputs.GetComponentsInChildren<Collider>();
        foreach (var c in colliders)
        {
            Physics.IgnoreCollision(creatorCollider, c, true);
        }
    }
    #endregion

    #region Callbacks
    /// <summary>
    /// Gets called when an active Path Point is not detected and when the VR button is pressed.
    /// </summary>
    public void AddPoint()
    {
        if (!_detector.IsTriggerStay)
        {
            var point = _pool.ActivatePoint(spawnPoint.position, spawnPoint.rotation);
            if (point != null)
            {
                _points.Add(point);
            }
            if (_points.Count > 1)
            {
                _cart.transform.position = _points[0].position;
                _cart.SetActive(true);
            }
        }
    }

    public void ActivateCart()
    {
        _isCartMoving = !_isCartMoving;
    }
    #endregion

    #region Reserved Functions
    /// <summary>
    /// Gets called when an active Path Point is detected and when the VR button is pressed.
    /// </summary>
    /// <param name="point"></param>
    public void RemovePoint(Transform point)
    {
        int index = _points.FindIndex(p => p == point);
        if (index != -1)
        {
            _pool.TerminatePoint(_points[index]);
            _points.RemoveAt(index);
        }

        if (_points.Count < 2)
        {
            _cart.SetActive(false);
        }
    }
    #endregion

    private void Update()
    {
        _viewer.ViewUpdate();
        if (!_isCartMoving)
        {
            if (_points.Count > 0)
            {
                _cartController.SetPosition(_points[0].position);
            }
            
        }
        else
        {
            StartCoroutine(CartMove());
            
        }
    }

    float tt = 0f;
    private IEnumerator CartMove()
    {
        if (isCor) yield break;
        isCor = true;

        while (tt < 1)
        {
            tt += Time.deltaTime * 0.1f;
            int p = _points.Count;
            switch (p)
            {
                /* Linear Bezier Curve */
                case 2:
                    _cart.transform.position = BezierCurves.LinearBezierCurves(
                            _points[0].position, _points[1].position, tt);
                    break;

                /* Quadratic Bezier Curve */
                case 3:
                    _cart.transform.position = BezierCurves.QuadraticBezierCurves(
                            _points[0].position, _points[1].position, _points[2].position, tt);
                    break;

                /* Cubic Bezier Curve */
                case var expression when p > 3:
                    for (int i = 0; i < SegmentCount(); i++)
                    {
                        Vector3[] segment = GetPointsInSegment(i);
                        _cart.transform.position = BezierCurves.CubicBezierCurves(
                                segment[0], segment[1], segment[2], segment[3], tt);
                    }
                    break;
            }
            yield return null;
        }
        tt = 0f;

        isCor = false;
    }

    // Duplicate
    #region Temp 
    private Vector3[] GetPointsInSegment(int i)
    {
        return new Vector3[] { _points[i * 3].position, _points[i * 3 + 1].position,
            _points[i * 3 + 2].position, _points[i * 3 + 3].position };
    }

    private int SegmentCount()
    {
        return (_points.Count - 4) / 3 + 1;
    }
    #endregion

    private void OnDisable()
    {
        _detector.PathPointDetectAction -= RemovePoint;
    }
}