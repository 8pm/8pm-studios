﻿using System.Collections.Generic;
using UnityEngine;

public class PathViewer
{
    #region Private Field
    private static Color anchorPointColor = new Color32(116, 255, 121, 255);
    private static Color controlPointColor = new Color32(0, 111, 10, 36);
    private static Color pathColor = new Color32(116, 255, 121, 255);
    private static Color controlPathColor = new Color32(255, 216, 116, 255);

    private LineRenderer _lr = null;
    private List<Transform> _points = null;
    private Material _pathMat = null;
    private Material _controlLineMat = null;
    private Material _anchorPointMat = null;
    private Material _controlPointMat = null;
    #endregion

    public PathViewer(List<Transform> points, Material pathMat, Material controlPathMat,
        Material anchorPointMat, Material controlPointMat)
    {
        _points = points;
        SetupPathLine(pathMat, controlPathMat);
        SetupPathPoint(anchorPointMat, controlPointMat);
    }

    #region Initialize
    private void SetupPathLine(Material path, Material controlLine)
    {
        _pathMat = path;
        _pathMat.color = pathColor;
        _controlLineMat = controlLine;
        _controlLineMat.color = controlPathColor;

        _lr = GameObject.Find("PathPool").AddComponent<LineRenderer>();
        _lr.positionCount = 200;
        _lr.startWidth = 0.06f;
        _lr.endWidth = 0.06f;
        _lr.material = _pathMat;
    }

    private void SetupPathPoint(Material anchor, Material control)
    {
        _anchorPointMat = anchor;
        _anchorPointMat.color = anchorPointColor;
        _controlPointMat = control;
        _controlPointMat.color = controlPointColor;
    }
    #endregion

    #region Draw Curves
    /// <summary>
    /// Called every frame on PathController Update.
    /// </summary>
    public void ViewUpdate()
    {
        DrawBezierCurve(_points.Count);
    }

    private void DrawBezierCurve(int points)
    {
        switch (points)
        {
            /* Linear Bezier Curve */
            case 2:
                float t = 0f;
                for (int i = 0; i < _lr.positionCount; i++)
                {
                    _lr.SetPosition(i, BezierCurves.LinearBezierCurves(
                        _points[0].position, _points[1].position, t));
                    t += (1 / (float)_lr.positionCount);
                }
                break;

            /* Quadratic Bezier Curve */
            case 3:
                t = 0f;
                for (int i = 0; i < _lr.positionCount; i++)
                {
                    _lr.SetPosition(i, BezierCurves.QuadraticBezierCurves(
                        _points[0].position, _points[1].position, _points[2].position, t));
                    t += (1 / (float)_lr.positionCount);
                }
                break;

            /* Cubic Bezier Curve */
            case var expression when points > 3:
                t = 0f;
                for (int i = 0; i < SegmentCount(); i++)
                {
                    var cp1 = _points[i * 3 + 1].GetComponent<Renderer>();
                    var cp2 = _points[i * 3 + 2].GetComponent<Renderer>();
                    cp1.material = _controlPointMat;
                    cp2.material = _controlPointMat;

                    Vector3[] segment = GetPointsInSegment(i);
                    for (int j = 0; j < _lr.positionCount; j++)
                    {
                        _lr.SetPosition(j, BezierCurves.CubicBezierCurves(
                            segment[0], segment[1], segment[2], segment[3], t));
                        t += (1 / (float)_lr.positionCount);
                    }
                }
                break;
        }
    }
    #endregion

    #region Helper Functions
    private Vector3[] GetPointsInSegment(int i)
    {
        return new Vector3[] { _points[i * 3].position, _points[i * 3 + 1].position,
            _points[i * 3 + 2].position, _points[i * 3 + 3].position };
    }
    
    private int SegmentCount()
    {
        return (_points.Count - 4) / 3 + 1;
    }
    #endregion
}