﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class EnvColor : MonoBehaviour
{
    [SerializeField, ColorUsage(true, true)]
    private Color setColor = Color.white;

    private Volume _volume = null;
    private ColorAdjustments _ca = null;
    private Color originColor;
    JY_Controller_0123 ctrl;

    private void OnEnable()
    {

        ctrl = GameObject.Find("RightHandAnchor").GetComponent<JY_Controller_0123>();

        //WhenFixExists();

        _volume = GameObject.Find("Global Volume").GetComponent<Volume>();
        _volume.profile.TryGet<ColorAdjustments>(out _ca);
        originColor = _ca.colorFilter.value;
        _ca.colorFilter.value = setColor;
    }

    //private void OnDisable()
    //{
    //    _ca.colorFilter.value = originColor;
    //    print("온디스에이블");
    //}

    private void WhenFixExists()
    {
        print("ctrlFix는: " + ctrl.fixedCol.name);
        if (ctrl.fixedCol != null)
        {
            originColor = ctrl.fixedCol.GetComponent<EnvColor>().setColor;
            print("originColor는: " + originColor.ToString());
        }
    }
}
