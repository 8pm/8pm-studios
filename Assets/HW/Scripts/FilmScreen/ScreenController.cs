﻿using System;
using System.Collections;
using System.Collections.Generic;
using Evereal.VideoCapture;
using TMPro;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.UI;

namespace FilmCam
{
    public class ScreenController : MonoBehaviour
    {
        #region Serialized Field
        [Header("Output Screen"), SerializeField]
        private Vector2 screenSize = new Vector2(1920, 1080);
        [SerializeField]
        private List<GameObject> screens = null;
        [Header("Record Information"), SerializeField]
        private TextMeshProUGUI cameraName = null;
        [SerializeField]
        private TextMeshProUGUI cameraLens = null;
        [SerializeField]
        private TextMeshProUGUI take = null;
        [SerializeField]
        private TextMeshProUGUI dateTime = null;
        [SerializeField]
        private TextMeshProUGUI frame = null;
        [SerializeField]
        private GameObject rec = null;
        [Header("Studio Information"), SerializeField]
        private StudioInfoPreset studioPreset = null;
        [Header("Lens Preset"), SerializeField]
        private LensPresets lensPreset = null;
        #endregion

        #region Private Field
        private FilmCameraController _controller = null;
        private FFmpegEncoder _encoder = null;
        private Camera _cam = null;
        private bool _isCoroutineRunning = false;
        private bool _isBlinking = false;
        #endregion

        #region Properties
        public RenderTexture NotRecordingRt { get; private set; } = null;
        public RenderTexture RecordingRt { get; private set; } = null;
        #endregion

        private void Awake()
        {
            _controller = GetComponentInParent<FilmCameraController>();
            _encoder = GetComponentInParent<FFmpegEncoder>();
            GenerateScreen();
        }

        private void OnEnable()
        {
            _controller.PrimaryButtonAction += Blink;
        }
        private void Start()
        {
            InitVariables();
            ActivateNotRecordingScreen();
        }

        #region Initialize
        private void InitVariables()
        {
            _cam = _controller.Cameras[0];
            cameraName.text = _controller.CameraName;
            frame.text = "30";
        }

        private void GenerateScreen()
        {
            NotRecordingRt = new RenderTexture((int)screenSize.x, (int)screenSize.y,
                        24, GraphicsFormat.R8G8B8A8_SRGB);
            NotRecordingRt.Create();
            RecordingRt = _encoder.GetRecordingRT();
        }
        #endregion

        #region Subscribed Functions
        /// <summary>
        /// 녹화를 시작하면 깜빡이게 한다.
        /// 녹화 기능이 담긴 PrimaryButtonAction에 연결.
        /// </summary>
        private void Blink()
        {
            _isBlinking = !_isBlinking;

            if (_isBlinking) rec.SetActive(true);
            else rec.SetActive(false);
        }
        #endregion

        #region Event Callbacks
        /// <summary>
        /// 녹화 시작시 CameraRecorder의 OnRecordStart 이벤트가 호출
        /// </summary>
        public void ActivateRecordingScreen()
        {
            RecordingRt = _encoder.GetRecordingRT();
            foreach (var screen in screens)
            {
                screen.GetComponent<Renderer>().material.mainTexture = RecordingRt;
            }
        }

        /// <summary>
        /// 녹화 중지, 취소시 CameraRecorder의 OnRecordStop, OnRecordCancel 이벤트가 호출
        /// </summary>
        public void ActivateNotRecordingScreen()
        {
            _cam.targetTexture = NotRecordingRt;

            foreach (var screen in screens)
            {
                screen.GetComponent<Renderer>().material.mainTexture = NotRecordingRt;
            }
        }
        #endregion

        private void Update()
        {
            if (_isBlinking)
            {
                StartCoroutine(Blinking());
            }
            UpdateScreenInfoEveryFrame();
        }

        #region Refresh Screen Information
        private void UpdateScreenInfoEveryFrame()
        {
            cameraLens.text = lensPreset.GetLensName(_cam.fieldOfView) + "mm";
            dateTime.text = DateTime.UtcNow.ToLocalTime().ToString("yyyy.MM.dd.HH.mm.ss");
            take.text = studioPreset.Production + "_" + studioPreset.Director +
                "_" + studioPreset.Scene + "_T" + studioPreset.TakeCount.ToString();
        }

        private IEnumerator Blinking()
        {
            if (_isCoroutineRunning) yield break;
            _isCoroutineRunning = true;

            var elapsedTime = 0f;
            var duration = 2f;
            var recText = rec.GetComponent<TextMeshProUGUI>();
            var recCircle = rec.transform.GetChild(0).GetComponent<Image>();

            while (elapsedTime < duration / 2)
            {
                elapsedTime += Time.deltaTime;
                var textColor = recText.color;
                var circleColor = recCircle.color;

                textColor.a = Mathf.Lerp(textColor.a, 0, elapsedTime / (duration / 2));
                circleColor.a = Mathf.Lerp(circleColor.a, 0, elapsedTime / (duration / 2));
                recText.color = textColor;
                recCircle.color = circleColor;
                yield return null;
            }
            elapsedTime = 0f;

            while (elapsedTime < duration / 2)
            {
                elapsedTime += Time.deltaTime;
                var textColor = recText.color;
                var circleColor = recCircle.color;

                textColor.a = Mathf.Lerp(textColor.a, 255, elapsedTime / (duration / 2));
                circleColor.a = Mathf.Lerp(circleColor.a, 255, elapsedTime / (duration / 2));
                recText.color = textColor;
                recCircle.color = circleColor;
                yield return null;
            }

            _isCoroutineRunning = false;
        }
        #endregion

        private void OnDisable()
        {
            _controller.PrimaryButtonAction -= Blink;
        }
    }
}