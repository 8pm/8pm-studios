﻿using UnityEngine;

namespace FilmCam
{
    [CreateAssetMenu(fileName = "Studio Preset", menuName = "ScriptableObjects/Studio", order = 2)]
    public class StudioInfoPreset : ScriptableObject
    {
        #region Serialized Field
        [SerializeField]
        private string studio = null;
        [SerializeField]
        private string production = null;
        [SerializeField]
        private string director = null;
        [SerializeField]
        private string scene = null;
        [SerializeField]
        private int takeCount = 0;
        #endregion

        #region Properties
        public string Studio => studio;
        public string Production => production;
        public string Director => director;
        public string Scene => scene;
        public int TakeCount => takeCount;
        #endregion

        #region Update Information
        /// <summary>
        /// 
        /// </summary>
        /// <param name="studio"></param>
        /// <param name="production"></param>
        /// <param name="director"></param>
        /// <param name="scene"></param>
        public void UpdateInfo(string studio = "null", string production = "null",
            string director = "null", string scene = "null")
        {
            this.studio = studio;
            this.production = production;
            this.director = director;
            this.scene = scene;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ResetTakeCount() => takeCount = 0;

        /// <summary>
        /// 
        /// </summary>
        public void IncreaseTakeCount() => takeCount += 1;
        #endregion
    }
}