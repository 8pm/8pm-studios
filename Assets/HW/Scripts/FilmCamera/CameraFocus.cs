﻿using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace FilmCam
{
    public class CameraFocus : MonoBehaviour
    {
        #region Serialized Field
        [Space, Header("Focus Settings")]
        [SerializeField, Range(0, 0.6f)]
        private float rayRadius = 0.1f;
        [SerializeField]
        private float rayLength = 100f;
        [SerializeField]
        private bool autoFocus = false;
        [SerializeField, Range(0, 3f)]
        private float focusTime = 0.6f;
        [SerializeField]
        private AnimationCurve focusAc = null;
        #endregion

        #region Private Field
        private FilmCameraController _controller = null;
        private DepthOfField _dof = null;
        private Camera _cam = null;
        private bool _isCoroutineRunning = false;
        #endregion

        private void Awake()
        {
            _controller = GetComponent<FilmCameraController>();
        }
        private void OnEnable()
        {
            // focus funcions using VR inputs
        }

        private void Start()
        {
            InitVariables();
        }

        #region Initialize
        private void InitVariables()
        {
            _cam = _controller.Cameras[0];
            var _volume = _cam.GetComponentInChildren<Volume>();
            _ = _volume.profile.TryGet(out _dof);
        }
        #endregion

        private void FixedUpdate()
        {
            if (autoFocus) Focus();
        }

        #region Subscribed Functions
        /// <summary>
        /// 
        /// </summary>
        private void Focus()
        {
            StartCoroutine(Focusing());
        }

        /// <summary>
        /// 
        /// </summary>
        private void AutoFocusOnOff()
        {
            autoFocus = !autoFocus;
            if (autoFocus)
                _controller.PrimaryButtonAction -= Focus;
            else
                _controller.PrimaryButtonAction += Focus;
        }

        /// <summary>
        /// 
        /// </summary>
        private void FocusOnOff()
        {
            _dof.active = !_dof.active;
        }
        #endregion

        #region Focus Functions
        private IEnumerator Focusing()
        {
            if (_isCoroutineRunning) yield break;
            _isCoroutineRunning = true;

            var ray = new Ray(_cam.transform.position, _cam.transform.forward);

            if (Physics.SphereCast(ray, rayRadius, out var hit, rayLength))
            {
                var hitDst = Vector3.Distance(_cam.transform.position, hit.point);
                var elapsedTime = 0f;

                while (elapsedTime <= focusTime)
                {
                    var lerpVal = Mathf.Lerp(_dof.focusDistance.value, hitDst,
                        focusAc.Evaluate(elapsedTime / focusTime));
                    _dof.focusDistance.value = lerpVal;
                    elapsedTime += Time.fixedDeltaTime;
                    yield return null;
                }
                _dof.focusDistance.value = hitDst;
            }
            _isCoroutineRunning = false;
        }
        #endregion

        private void OnDisable()
        {
            // focus funcions using VR inputs
        }
    }
}