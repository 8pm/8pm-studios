﻿using UnityEngine;

public class CameraStabilizer : MonoBehaviour
{
    [Header("Camera"), SerializeField]
    private Transform filmCam = null;
    [SerializeField]
    private Transform camAnchor = null;
    [Header("Stabilizer"), SerializeField]
    private float positionStabilizeValue = 0.6f;
    [SerializeField]
    private float rotationStabilizeValue = 0.6f;
    
    void Update()
    {
        var positionOffset = Vector3.Distance(camAnchor.position, filmCam.position) + 1;
        filmCam.position = Vector3.Lerp(filmCam.position, camAnchor.position,
            positionOffset * Time.deltaTime * positionStabilizeValue * 10);

        var rotationOffset = Quaternion.Angle(camAnchor.rotation, filmCam.rotation);
        filmCam.rotation = Quaternion.Lerp(filmCam.rotation, camAnchor.rotation,
            rotationOffset * Time.deltaTime * rotationStabilizeValue / 1.5f);
    }
}
