﻿#define PC

using UnityEngine;

namespace FilmCam
{
    public class FilmCameraController : MonoBehaviour
    {
        #region Serialized Field
        [Header("Studio Information"), SerializeField]
        private StudioInfoPreset studioPreset = null;
        [Header("Cameras"), SerializeField]
        private string cameraName = null;
        [SerializeField]
        private GameObject cameraRoot = null;
        [Header("VR Inputs"), SerializeField]
        private Transform inputs = null;
        #endregion

        #region Delegates
        public delegate void FilmCamButton();
        public delegate void FilmCamJoystick(float val);

        public FilmCamButton PrimaryButtonAction;
        public FilmCamButton SecondaryButtonAction;
        public FilmCamButton MenuButtonAction;
        public FilmCamJoystick JoystickAction;
        #endregion

        #region Properties
        public Camera[] Cameras { get; private set; } = null;
        public string CameraName => cameraName;
        public FilmCameraManager CameraManager { get; private set; } = null;
        public VideoPlayerManager VideoPlayerManager { get; private set; } = null;
        #endregion

        private void Awake()
        {
            IgnoreCollisions();
            InitCameraSettings();
        }

        private void OnEnable()
        {
            var controlDesk = GameObject.FindGameObjectWithTag("ControlDesk");
            CameraManager = controlDesk.GetComponent<FilmCameraManager>();
            CameraManager.AddFilmCamera(gameObject);
            VideoPlayerManager = controlDesk.GetComponent<VideoPlayerManager>();
        }

        #region Initialize
        private void IgnoreCollisions()
        {
            var camCollider = GetComponent<Collider>();
            Collider[] colliders = inputs.GetComponentsInChildren<Collider>();
            foreach (var c in colliders)
            {
                Physics.IgnoreCollision(camCollider, c, true);
            }
        }

        private void InitCameraSettings()
        {
            Cameras = cameraRoot.GetComponentsInChildren<Camera>();
            foreach (var cam in Cameras)
            {
                cam.stereoTargetEye = StereoTargetEyeMask.None;
            }
        }
        #endregion

        #region VR Button Callbacks
        /// <summary>
        /// Called on OnButtonDown of VR inner button
        /// </summary>
        public void PrimaryButtonDown()
        {
            PrimaryButtonAction?.Invoke();
            studioPreset.IncreaseTakeCount();
        }

        /// <summary>
        /// Called on OnButtonDown of VR inner button
        /// </summary>
        public void SecondaryButtonDown()
        {
            SecondaryButtonAction?.Invoke();
        }

        /// <summary>
        /// Called on OnButtonDown of VR inner button
        /// </summary>
        public void MenuButtonDown()
        {
            MenuButtonAction?.Invoke();
        }

        /// <summary>
        /// Called on OnJoystickChange of VR joystick
        /// </summary>
        /// <param name="x"></param>
        /// <param name="z"></param>
        public void JoystickValueChange(float x, float z)
        {
            if (Mathf.Abs(z - 50f) > 16f)
            {
                var zVal = Mathf.Clamp(z - 50, -1, 1);
                JoystickAction?.Invoke(zVal);
            }
        }
        #endregion

        private void Update()
        {
#if PC
            if (Input.GetKeyDown(KeyCode.Q))
            {
                PrimaryButtonAction?.Invoke();
                studioPreset.IncreaseTakeCount();
            }
            else if (Input.GetKeyDown(KeyCode.W))
            {
                SecondaryButtonAction?.Invoke();
            }
            else if (Input.GetKeyDown(KeyCode.E))
            {
                MenuButtonAction?.Invoke();
                studioPreset.UpdateInfo("a", "a");
            }

            if (Input.mouseScrollDelta.y > 0)
            {
                JoystickAction(- 1);
            }
            if (Input.mouseScrollDelta.y < 0)
            {
                JoystickAction(1);
            }
#endif
        }
    }
}