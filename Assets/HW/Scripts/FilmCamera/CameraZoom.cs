﻿using UnityEngine;

namespace FilmCam
{
    public class CameraZoom : MonoBehaviour
    {
        #region Serialized Field
        [SerializeField]
        private LensPresets lensPresets = null;
        [SerializeField]
        private float zoomAmount = 6f;
        [SerializeField]
        private bool useFixedLens = false;
        #endregion

        #region Private Field
        private FilmCameraController _controller = null;
        private Camera _cam = null;
        #endregion

        private void Awake()
        {
            _controller = GetComponent<FilmCameraController>();
        }

        private void OnEnable()
        {
            if (useFixedLens)
                _controller.JoystickAction += FixedLens;
            else
                _controller.JoystickAction += Zoom;
        }

        private void Start()
        {
            InitVariables();
        }

        #region Initialize
        private void InitVariables()
        {
            _cam = _controller.Cameras[0];
            lensPresets.CreateDefaultLens();
        }
        #endregion

        #region Subscribed Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dir"></param>
        private void Zoom(float dir)
        {
            var fov = _cam.fieldOfView + dir * zoomAmount * Time.deltaTime;
            _cam.fieldOfView = Mathf.Clamp(fov,
                lensPresets.Lens[lensPresets.Lens.Length - 1].FoV, lensPresets.Lens[0].FoV);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dir"></param>
        private void FixedLens(float dir)
        {
            var fov = _cam.fieldOfView + dir * zoomAmount * Time.deltaTime;
            var fovClamp = Mathf.Clamp(fov,
                lensPresets.Lens[lensPresets.Lens.Length - 1].FoV, lensPresets.Lens[0].FoV);
            var index = lensPresets.GetLensIndex(fovClamp);
            _cam.fieldOfView = lensPresets.Lens[index].FoV;
        }

        /// <summary>
        /// 
        /// </summary>
        private void FixedLensOnOff()
        {
            useFixedLens = !useFixedLens;
            if (useFixedLens)
            {
                _controller.JoystickAction += FixedLens;
                _controller.JoystickAction -= Zoom;
            }
            else
            {
                _controller.JoystickAction += Zoom;
                _controller.JoystickAction -= FixedLens;
            }
        }
        #endregion

        private void OnDisable()
        {
            if (useFixedLens)
                _controller.JoystickAction -= FixedLens;
            else
                _controller.JoystickAction -= Zoom;
        }
    }
}