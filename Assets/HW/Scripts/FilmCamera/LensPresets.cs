﻿using System.Collections.Generic;
using UnityEngine;

namespace FilmCam
{
    [CreateAssetMenu(fileName = "Lens Presets", menuName = "ScriptableObjects/Lens", order = 1)]
    public class LensPresets : ScriptableObject
    {
        #region Private Field
        private LensPreset[] _lens = new LensPreset[0];
        private bool _isInitialized = false;
        private float _threshold = 3f;
        #endregion

        #region Properties
        public LensPreset[] Lens => _lens;
        #endregion

        private void OnEnable()
        {
            _isInitialized = false;
        }

        #region Initialize
        /// <summary>
        /// This function should be called on Start.
        /// </summary>
        public void CreateDefaultLens()
        {
            if (!_isInitialized)
            {
                List<LensPreset> defaults = new List<LensPreset>();
                defaults.Add(new LensPreset() { Name = "21mm", FoV = 60f });
                defaults.Add(new LensPreset() { Name = "35mm", FoV = 38f });
                defaults.Add(new LensPreset() { Name = "58mm", FoV = 23f });
                defaults.Add(new LensPreset() { Name = "80mm", FoV = 17f });
                defaults.Add(new LensPreset() { Name = "125mm", FoV = 10f });
                _lens = defaults.ToArray();
                _isInitialized = true;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get the index of the lens preset that matches the fov value.
        /// </summary>
        /// <param name="fov"></param>
        /// <returns></returns>
        public int GetLensIndex(float fov)
        {
            for (int i = 0; i < _lens.Length; ++i)
            {
                if (Mathf.Abs(_lens[i].FoV) - Mathf.Abs(fov) <= _threshold) return i;
            }
            return -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fov"></param>
        /// <returns></returns>
        public string GetLensName(float fov)
        {
            var perc = Map(10, 60, 125, 21, fov);
            return perc.ToString();
        }

        private float Map(float a1, float a2, float b1, float b2, float s)
        {
            return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
        }
        #endregion

        public struct LensPreset
        {
            #region Private Field
            [Tooltip("Lens Name")]
            private string _name;

            [Range(1f, 179f)]
            private float _fieldOfView;
            #endregion

            #region Properties
            public string Name { get => _name; set => _name = value; }
            public float FoV { get => _fieldOfView; set => _fieldOfView = value; }
            #endregion
        }
    }
}