﻿using Evereal.VideoCapture;
using UnityEngine;
using UnityEngine.Events;

namespace FilmCam
{
    public class CameraRecorder : MonoBehaviour
    {
        #region Events
        public UnityEvent OnRecordStart = null;
        public UnityEvent OnRecordStop = null;
        public UnityEvent OnRecordCancel = null;
        #endregion

        #region Private Field
        private VideoCapture _videoCapture = null;
        private FilmCameraController _controller = null;

        private bool _isCapturing = false;
        private bool _isCanceled = false;
        private bool _previousState = false;
        #endregion

        private void Awake()
        {
            _videoCapture = GetComponent<VideoCapture>();
            _controller = GetComponent<FilmCameraController>();
            Application.runInBackground = true;
        }

        private void OnEnable()
        {
            _controller.PrimaryButtonAction += ToggleVideoCapture;
        }

        private void Start()
        {
            FFmpegMuxer.singleton.OnComplete += RefreshVideoList;
        }

        #region Subscribed Functions
        /// <summary>
        /// 
        /// </summary>
        private void ToggleVideoCapture()
        {
            _isCapturing = !_isCapturing;
        }

        /// <summary>
        /// Called after video combine.
        /// </summary>
        public void RefreshVideoList(string s)
        {
            _controller.VideoPlayerManager.UpdateVideoList();
            _previousState = false;
        }
        #endregion

        private void Update()
        {
            Record(_isCapturing, ref _previousState);
            CancelRecord(ref _isCanceled, ref _isCapturing);
        }

        #region Record Functions
        private void Record(bool recording, ref bool previous)
        {
            if (recording && !previous)
            {
                _videoCapture.StartCapture();
                OnRecordStart?.Invoke();
                if (!previous) previous = true;
            }
            else if (!recording && previous)
            {
                _videoCapture.StopCapture();
                OnRecordStop?.Invoke();
            }
        }

        private void CancelRecord(ref bool canceled, ref bool capturing)
        {
            if (canceled)
            {
                _videoCapture.CancelCapture();
                OnRecordCancel?.Invoke();
                canceled = false;
                capturing = false;
            }
        }
        #endregion

        private void OnDisable()
        {
            _controller.PrimaryButtonAction -= ToggleVideoCapture;
            FFmpegMuxer.singleton.OnComplete -= RefreshVideoList;
        }
    }
}