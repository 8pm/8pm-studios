﻿using System;
using Evereal.VideoCapture;
using TMPro;
using UnityEngine;

namespace FilmCam
{
    public class ClapperBoardController : MonoBehaviour
    {
        #region Serialized Field
        [Header("Take Information"), SerializeField]
        private TextMeshProUGUI studioInfo = null;
        [SerializeField]
        private TextMeshProUGUI productionInfo = null;
        [SerializeField]
        private TextMeshProUGUI directorInfo = null;
        [SerializeField]
        private TextMeshProUGUI cameraInfo = null;
        [SerializeField]
        private TextMeshProUGUI dateInfo = null;
        [SerializeField]
        private TextMeshProUGUI sceneInfo = null;
        [SerializeField]
        private TextMeshProUGUI takeInfo = null;
        [SerializeField]
        private float delayTime = 1.6f;
        [Header("Studio Information"), SerializeField]
        private StudioInfoPreset studioPreset = null;
        #endregion

        #region Private Field
        private Animator _animator = null;
        private FilmCameraController _controller = null;
        private bool _canClap = true;
        #endregion

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _controller = GetComponentInParent<FilmCameraController>();
        }

        private void OnEnable()
        {
            _controller.PrimaryButtonAction += Clap;
        }

        private void Start()
        {
            FFmpegMuxer.singleton.OnComplete += CanClap;

            cameraInfo.text = _controller.CameraName;
            gameObject.SetActive(false);
        }

        #region Subscribed Functions
        /// <summary>
        /// 녹화 시작시 한번 호출되고 기록 내용을 업데이트한다.
        /// </summary>
        private void Clap()
        {
            if (!_canClap) return;
            gameObject.SetActive(true);

            studioInfo.text = studioPreset.Studio;
            productionInfo.text = studioPreset.Production;
            directorInfo.text = studioPreset.Director;
            dateInfo.text = DateTime.UtcNow.ToLocalTime().ToString("yyyy.MM.dd");
            sceneInfo.text = studioPreset.Scene;
            takeInfo.text = studioPreset.TakeCount.ToString();

            Invoke("Action", delayTime);
        }

        private void Action() => _animator.SetTrigger("action");

        /// <summary>
        /// This Funcion is called when the recording stops.
        /// </summary>
        public void CanClap(string s)
        {
            _canClap = true;
        }
        #endregion

        #region Event Callbacks
        /// <summary>
        /// This function is called at the end of the 'ReadyAction' animation.
        /// </summary>
        public void Rolling()
        {
            // add affect in future development
            _canClap = false;
            Invoke("HideClapperBoard", delayTime);
        }

        private void HideClapperBoard() => gameObject.SetActive(false);
        #endregion
    }
}