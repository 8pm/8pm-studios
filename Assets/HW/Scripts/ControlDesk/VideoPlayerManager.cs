﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Video;

namespace FilmCam
{
    public class VideoPlayerManager : MonoBehaviour
    {
        #region Private Field
        private static string CAPTURE_PATH = null;

        private ControlDeskSystem _deskController = null;
        private VideoPlayer _videoPlayer = null;
        private List<string> _videoNames = null;
        private int _videoIndex = 0;
        private bool _isLoading = false;
        private ControlMode _currentMode = ControlMode.VideoViewer;
        #endregion

        private void Awake()
        {
            CAPTURE_PATH = Application.dataPath + "/Captures/";
            _deskController = GetComponent<ControlDeskSystem>();
            _videoPlayer = _deskController.GetVideoPlayer();
        }

        private void OnEnable()
        {
            _deskController.ModeSwitchAction += OnModeChange;
        }

        private void Start()
        {
            CreateCaptureFolder();
            UpdateVideoList();
            PlayNew(0);
        }

        #region Initialize
        private void CreateCaptureFolder()
        {
            if (!Directory.Exists(CAPTURE_PATH))
            {
                Directory.CreateDirectory(CAPTURE_PATH);
            }
        }
        #endregion

        #region Subsrcibed Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode"></param>
        private void OnModeChange(ControlMode mode)
        {
            _currentMode = mode;
            if (_currentMode.Equals(ControlMode.VideoViewer))
            {
                PlayNew(0);
                _videoIndex = 0;

                _deskController.PreviousAction += ShowPreviousVideo;
                _deskController.NextAction += ShowNextVideo;
                _deskController.PrimaryAction += PlayStopVideo;
                _deskController.SecondaryAction += DeleteVideo;
            }
            else
            {
                _deskController.PreviousAction -= ShowPreviousVideo;
                _deskController.NextAction -= ShowNextVideo;
                _deskController.PrimaryAction -= PlayStopVideo;
                _deskController.SecondaryAction -= DeleteVideo;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ShowPreviousVideo()
        {
            if (_videoIndex == 0) return;
            _videoIndex -= 1;

            PlayNew(_videoIndex);
        }

        /// <summary>
        /// 
        /// </summary>
        private void ShowNextVideo()
        {
            if (_videoIndex >= _videoNames.Count - 1) return;
            _videoIndex += 1;

            PlayNew(_videoIndex);
        }

        /// <summary>
        /// 
        /// </summary>
        private void PlayStopVideo()
        {
            PlayPause();
        }

        /// <summary>
        /// 
        /// </summary>
        private void DeleteVideo()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        private void DeleteVideoConfirm()
        {

        }
        #endregion

        #region Video Controls
        /// <summary>
        /// Update video name list.
        /// 영상이 저장되면 호출된다.
        /// </summary>
        public void UpdateVideoList()
        {
            var fileNames = Directory.GetFiles(CAPTURE_PATH, "*.mp4");
            print(fileNames);
            Array.Sort(fileNames);

            if (_videoNames == null)
            {
                _videoNames = new List<string>();
            }
            else if (_videoNames.Count > 0)
            {
                _videoNames.Clear();
            }

            foreach (var name in fileNames)
            {
                _videoNames.Add(Path.GetFileNameWithoutExtension(name));
            }
        }

        private void PlayPause()
        {
            if (_videoPlayer.isPlaying)
            {
                _videoPlayer.Pause();
            }
            else
            {
                _videoPlayer.Play();
            }
        }

        private void PlayNew(int index)
        {
            if (_videoPlayer.isPlaying)
            {
                _videoPlayer.Stop();
            }

            if (_videoNames.Count > index)
            {
                StartCoroutine(LoadVideo(_videoNames[index]));
            }
        }

        private IEnumerator LoadVideo(string name)
        {
            if (_isLoading) yield break;
            _isLoading = true;

            _videoPlayer.url = CAPTURE_PATH + name + ".mp4";

            //ResourceRequest request = Resources.LoadAsync<VideoClip>("Captures\\" + name);
            //while (!request.isDone)
            //{
            //    Debug.Log("Load progress: " + request.progress);
            //    yield return null;
            //}
            //VideoClip clip = request.asset as VideoClip;
            //print(clip);
            //_videoPlayer.clip = clip;

            //Resources.UnloadUnusedAssets();

            _isLoading = false;
        }
        #endregion

        private void OnDisable()
        {
            _deskController.ModeSwitchAction -= OnModeChange;
            if (_currentMode.Equals(ControlMode.VideoViewer))
            {
                _deskController.PreviousAction += ShowPreviousVideo;
                _deskController.NextAction += ShowNextVideo;
                _deskController.PrimaryAction += PlayStopVideo;
                _deskController.SecondaryAction += DeleteVideo;
            }
            else
            {
                _deskController.PreviousAction -= ShowPreviousVideo;
                _deskController.NextAction -= ShowNextVideo;
                _deskController.PrimaryAction -= PlayStopVideo;
                _deskController.SecondaryAction -= DeleteVideo;
            }
        }
    }
}
