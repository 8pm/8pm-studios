﻿using Evereal.VideoCapture;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace FilmCam
{
    public class FilmCameraManager : MonoBehaviour
    {
        #region Events
        public UnityEvent OnMultiRecordStart = null;
        public UnityEvent OnMultiRecordStop = null;
        public UnityEvent OnMultiRecordCancel = null;
        #endregion

        #region Private Field
        private ControlDeskSystem _deskController = null;
        private VideoCaptureManager _videoCaptureManager = null;
        private List<GameObject> _filmCameras = new List<GameObject>();
        private List<int> _selectedCam = new List<int>();
        private Transform _viewScreen = null; 

        private int _camIndex = 0;
        private bool _isMultiCapturing = false;
        private bool _isCanceled = false;
        private bool _previousState = false;
        private ControlMode _currentMode = ControlMode.VideoViewer;
        #endregion

        private void Awake()
        {
            _deskController = GetComponent<ControlDeskSystem>();
            _videoCaptureManager = GetComponent<VideoCaptureManager>();
        }

        private void OnEnable()
        {
            _deskController.ModeSwitchAction += OnModeChange;
        }

        private void Start()
        {
            InitVariable();
            ShowScreen(0);
        }

        #region Initialize
        private void InitVariable()
        {
            _viewScreen = _deskController.OutputSource.GetChild(0);
        }
        #endregion

        #region Subscribed Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode"></param>
        private void OnModeChange(ControlMode mode)
        {
            _currentMode = mode;
            if (_currentMode.Equals(ControlMode.CameraViewer))
            {
                _deskController.PreviousAction += ShowPreviousCam;
                _deskController.NextAction += ShowNextCam;
                _deskController.PrimaryAction += RecordSelectedCam;
                _deskController.SecondaryAction += SelectCam;
            }
            else
            {
                _deskController.PreviousAction -= ShowPreviousCam;
                _deskController.NextAction -= ShowNextCam;
                _deskController.PrimaryAction -= RecordSelectedCam;
                _deskController.SecondaryAction -= SelectCam;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ShowPreviousCam()
        {
            if (_camIndex == 0) return;
            _camIndex -= 1;

            ShowScreen(_camIndex);
        }

        /// <summary>
        /// 
        /// </summary>
        private void ShowNextCam()
        {
            if (_camIndex >= _filmCameras.Count - 1) return;
            _camIndex += 1;

            ShowScreen(_camIndex);
        }

        /// <summary>
        /// 
        /// </summary>
        private void RecordSelectedCam()
        {
            var videoCaps = new List<VideoCapture>();
            foreach (var s in _selectedCam)
            {
                videoCaps.Add(_filmCameras[s].GetComponent<VideoCapture>());
            }
            _videoCaptureManager.videoCaptures = videoCaps.ToArray();
            _isMultiCapturing = !_isMultiCapturing;
        }

        /// <summary>
        /// 
        /// </summary>
        private void SelectCam()
        {
            if (_selectedCam.Contains(_camIndex))
            {
                _selectedCam.Remove(_camIndex);
            }
            else
            {
                _selectedCam.Add(_camIndex);
            }
        }
        #endregion

        #region Screen Controls
        /// <summary>
        /// Add a newly instantiated film camera to the list.
        /// </summary>
        public void AddFilmCamera(GameObject filmCam)
        {
            _filmCameras.Add(filmCam);
        }

        private void ShowScreen(int index)
        {
            if (_filmCameras.Count > index)
            {
                var sc = _filmCameras[index].GetComponentInChildren<ScreenController>();
                _viewScreen.GetComponent<Renderer>().material.mainTexture = sc.NotRecordingRt;
            }
        }
        #endregion

        private void Update()
        {
            MultiRecord(_isMultiCapturing, ref _previousState);
            CancelMultiRecord(ref _isCanceled, ref _isMultiCapturing);
        }

        #region Record Functions
        private void MultiRecord(bool record, ref bool previous)
        {
            if (record && !previous)
            {
                _videoCaptureManager.StartCapture();
                OnMultiRecordStart?.Invoke();
                
                // not working
                var sc = _filmCameras[_camIndex].GetComponentInChildren<ScreenController>();
                _viewScreen.GetComponent<Renderer>().material.mainTexture = sc.RecordingRt;
                
                if (!previous) previous = true;
            }
            else if (!record && previous)
            {
                _videoCaptureManager.StopCapture();
                OnMultiRecordStop?.Invoke();

                // not norking
                var sc = _filmCameras[_camIndex].GetComponentInChildren<ScreenController>();
                _viewScreen.GetComponent<Renderer>().material.mainTexture = sc.NotRecordingRt;
            }
        }

        private void CancelMultiRecord(ref bool canceled, ref bool record)
        {
            if (canceled)
            {
                _videoCaptureManager.CancelCapture();
                OnMultiRecordCancel?.Invoke();
                canceled = false;
                record = false;
            }
        }
        #endregion

        private void OnDisable()
        {
            _deskController.ModeSwitchAction -= OnModeChange;
            if (_currentMode.Equals(ControlMode.CameraViewer))
            {
                _deskController.PreviousAction += ShowPreviousCam;
                _deskController.NextAction += ShowNextCam;
                _deskController.PrimaryAction += RecordSelectedCam;
                _deskController.SecondaryAction += SelectCam;
            }
            else
            {
                _deskController.PreviousAction -= ShowPreviousCam;
                _deskController.NextAction -= ShowNextCam;
                _deskController.PrimaryAction -= RecordSelectedCam;
                _deskController.SecondaryAction -= SelectCam;
            }
        }
    }
}