﻿using System;
using UnityEngine;
using UnityEngine.Video;

namespace FilmCam
{
    public class ControlDeskSystem : MonoBehaviour
    {
        #region Serialized Field
        [Header("Desk"), SerializeField]
        private Collider deskCollider = null;
        [Header("Output"), SerializeField]
        private Transform outputSource = null;
        [Header("Inputs"), SerializeField]
        private GameObject rootInput = null;
        #endregion

        #region Delegates
        public delegate void ModeControlDeskButton(ControlMode mode);
        public delegate void ControlDeskButton();

        public ModeControlDeskButton ModeSwitchAction = null;
        public ControlDeskButton PreviousAction = null;
        public ControlDeskButton NextAction = null;
        public ControlDeskButton PrimaryAction = null;
        public ControlDeskButton SecondaryAction = null;
        #endregion

        #region Pirvate Field
        private ControlMode _currentMode = ControlMode.VideoViewer;
        #endregion

        #region Properties
        public Transform OutputSource { get => outputSource; }
        #endregion

        private void Awake()
        {
            IgnoreCollions();
        }

        #region Initialize
        private void IgnoreCollions()
        {
            var colliders = rootInput.GetComponentsInChildren<Collider>();
            foreach (var c in colliders)
            {
                Physics.IgnoreCollision(deskCollider, c, true);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public VideoPlayer GetVideoPlayer()
        {
            return outputSource.GetComponentInChildren<VideoPlayer>();
        }
        #endregion

        private void Start()
        {
            if (outputSource != null)
            {
                ModeSwitchButtonDown(); // Setting the initial state
            }
            else
            {
                Debug.LogError("Output source is null.");
            }
        }

        #region VR Button Callbacks
        /// <summary>
        /// 
        /// </summary>
        public void ModeSwitchButtonDown()
        {
            if (outputSource != null)
            {
                NextControlMode(ref _currentMode);
            }
            else
            {
                Debug.LogError("Output source is null.");
            }
            ModeSwitchAction?.Invoke(_currentMode);
        }

        /// <summary>
        /// 
        /// </summary>
        public void PreviousButtonDown()
        {
            PreviousAction?.Invoke();
        }

        /// <summary>
        /// 
        /// </summary>
        public void NextButtonDown()
        {
            NextAction?.Invoke();
        }

        /// <summary>
        /// 
        /// </summary>
        public void PrimaryButtonDown()
        {
            PrimaryAction?.Invoke();
        }

        /// <summary>
        /// 
        /// </summary>
        public void SecondaryButtonDown()
        {
            SecondaryAction?.Invoke();
        }
        #endregion

        #region Mode Controls
        /// <summary>
        /// 컨트롤 모드를 다음 모드로 변경.
        /// 아웃풋 스크린도 변경.
        /// </summary>
        /// <param name="current"></param>
        private void NextControlMode(ref ControlMode current)
        {
            int index = (int)current + 1;
            outputSource.GetChild((int)current).gameObject.SetActive(false);

            if (index >= Enum.GetNames(typeof(ControlMode)).Length) index = 0;

            outputSource.GetChild(index).gameObject.SetActive(true);
            current = (ControlMode)index;
        }
        #endregion
    }

    public enum ControlMode { CameraViewer, VideoViewer }
}