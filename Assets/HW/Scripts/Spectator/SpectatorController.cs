﻿using System.Collections.Generic;
using UnityEngine;

public class SpectatorController : MonoBehaviour
{
    #region Serialized Field
    [Header("Input"), SerializeField]
    private KeyCode switchCameraKeycode = KeyCode.Space;
    [Header("Spectator Camera"), SerializeField]
    private Transform spectatorCam = null;
    [SerializeField]
    private List<GameObject> attachPoints = null;
    [Header("Smoothing"), SerializeField]
    private float smoothPos = 0.1f;
    [SerializeField]
    private float smoothRot = 0.1f;
    #endregion

    #region Private Field
    private Transform _currentTransform = null;
    private int _currentCameraIndex = 0;
    #endregion

    private void Awake()
    {
        if (attachPoints.Count > 0) SwitchCamera(0);
    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            SwitchCamera();
        }
    }

    private void LateUpdate()
    {
        if (_currentTransform)
        {
            var moveSpeed = Mathf.Clamp(1f - smoothPos, 0f, 1f);
            spectatorCam.position = Vector3.Lerp(spectatorCam.position,
                _currentTransform.position, moveSpeed);

            var rotSpeed = Mathf.Clamp(1f - smoothRot, 0f, 1f);
            spectatorCam.rotation = Quaternion.Slerp(spectatorCam.rotation,
                _currentTransform.rotation, rotSpeed);
        }
    }

    #region Switch Spectator Camera
    public void SwitchCamera()
    {
        SwitchCamera(_currentCameraIndex + 1);
    }

    private void SwitchCamera(int index)
    {
        // Disable previous camera
        var previousPoint = attachPoints[_currentCameraIndex];
        if (previousPoint.TryGetComponent<SpectatorEye>(out var previousEye))
        {
            previousEye.SetSpectatorEye(false);
        }
        _currentCameraIndex = index % attachPoints.Count;

        // Enable new camera
        var newPoint = attachPoints[_currentCameraIndex];
        if (newPoint.TryGetComponent<SpectatorEye>(out var newEye))
        {
            newEye.SetSpectatorEye(true);
            _currentTransform = newPoint.transform.GetChild(0);
        }

        spectatorCam.position = _currentTransform.position;
        spectatorCam.rotation = _currentTransform.rotation;
    }
    #endregion
}
