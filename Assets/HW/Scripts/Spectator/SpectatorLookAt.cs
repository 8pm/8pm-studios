﻿using UnityEngine;

public class SpectatorLookAt : MonoBehaviour
{
    #region Serialized Field
    [SerializeField]
    private Transform target = null;

    #endregion

    private void Update()
    {
        transform.LookAt(target.position);
    }
}
