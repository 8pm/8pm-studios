# 🎬8pm Studios
## 아마추어 영화인들을 네트워킹해 작품을 만들 수 있도록 하는 VR 영화 촬영 플랫폼
#### 기능: 촬영, 소품 및 배경 선택, 네트워크, 바디 트래킹
#### 해당 프로젝트의 기능을 사용해 영화 <아저씨>의 한 장면을 따라 촬영한 [결과물 영상][영상 링크]
[영상 링크]: https://drive.google.com/file/d/1lacO8nI2sl8xKDCJ5G1O6BLhUamxIB1a/view?usp=sharing
#### 한국전파진흥협회 ML-Agents 기반 실감콘텐츠 개발 과정 우수 프로젝트상 수상
